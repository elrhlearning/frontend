## Description

PlanetReview - Angular Frontend app that use NestJS APi [NestJS APi](https://bitbucket.org/elrhlearning/backend/src/master/ "NestJS APi")

## Preview

App deployed in Heroku Cloud :  [PlanetReview](https://planetreviews.herokuapp.com/ "PlanetReview")

## Features

*  Dependency Injection
*  Modular app
*  Guards for authentification
*  JWT token-based authentication
*  HTTP Interceptor used to send JWT Token in each request 
*  HTTP Interceptor to handle Rxjs error 
*  Unit testing of each Component using Karma and TestBed
*  Unit testing of each Service using Karma, TestBed and HttpTestingController using dummy data comes from  TestServiceFactory 
*  Socket.io Client used for : Notification - Conversation between users
*  100% responsive : Angular FlexLayout
*  Socket.io Client for realtime interaction (Conversation between users - notifications)
*  validatorjs : library used with to create custom Validators used in forms (example : Validator for website link)

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).