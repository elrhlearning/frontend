const host : string = 'https://planetreviews-api.herokuapp.com/';
const webSocketHost : string = host;
export const environment = {
  production: true,
  backendHost : host,
  apiURL : host + 'api/v1/',
  assetsPath : host + 'v1/',
  webSocketHost : webSocketHost,
  messengerWebSocketHost : `${webSocketHost}messenger`,
  // config for error handling
  handleErrorsWithSnackbar : true
};