import { Review } from '../../modules/shared/entity/review.entity';
import { Image } from '../../modules/shared/entity/image.entity';
import { Comment } from '../../modules/shared/entity/comment.entity';
import { GetReviewsParams, ReviewService } from '../../modules/reviews/services/review.service';
import { TestServiceFactory } from './test-services.factory';
import { of } from 'rxjs';
export class ReviewServiceMock { 
    private reviews : Review[];
    private comments : Comment[] = [];
    getAll(params? : GetReviewsParams){
        if(!this.reviews){
            this.reviews = [];
            for(let i = 1;i<= 20;i++){
                let r = new Review();
                r.id = i;
                r.dateCreation = new Date().toDateString();
                r.link = `www.review${i}.com`;
                r.description = `review${i} description`;
                r.title = `Review${i} Title`;
                // img
                r.image = new Image();
                r.image.id = i;
                r.image.name = `img_review${i}`;
                r.image.path = `img_review${i}_path`;
                // user
                r.user = TestServiceFactory.getUserServiceMock().getRandomUser();
                // comments
                for(let j = 1;j<= 3; j++){
                    let c = new Comment();
                    c.content = `Comment Test ${j} of review ${j}`;
                    c.dateCreation = new Date().toDateString();
                    c.review = r;
                    c.owner = TestServiceFactory.getUserServiceMock().getRandomUser();
                    this.comments.push(c);
                }
                this.reviews.push(r);
            }
        }
        this.reviews;
    }

    getRandomComment() : Comment {
        this.getAll();
        return this.comments[TestServiceFactory.getRandomInt(0,(this.comments.length-1))];
    }
    
    getRandomReview() : Review {
        this.getAll();
        return this.reviews[TestServiceFactory.getRandomInt(0,(this.reviews.length-1))];
    }
}


