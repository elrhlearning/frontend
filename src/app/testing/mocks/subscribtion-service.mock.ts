import { Subscribtion } from '../../modules/shared/entity/subscribtion.entity';
import { TestServiceFactory } from './test-services.factory';
export class MockSubscriptionService{
    private  subscriptions : Subscribtion[];


    getListOfSubscriptions() : Subscribtion[]{
        if(!this.subscriptions){
            this.subscriptions = [];
            for(let i = 1; i <=10; i++ ){
                let subs = new Subscribtion();
                subs.dateCreation = new Date().toISOString();
                subs.id = i;
                subs.review = TestServiceFactory.getReviewsServiceMock().getRandomReview();
                if(i%2 == 0)
                    subs.subscribed = TestServiceFactory.getUserServiceMock().getRandomUser();
                else 
                    subs.subscribed = TestServiceFactory.getUserServiceMock().getConnectedUser();
                this.subscriptions.push(subs);
            }
        }
        return this.subscriptions;
   }


   getConnectedUserSubscriptions() : Subscribtion[] {
       return this.getListOfSubscriptions().filter((sub : Subscribtion) => {
           return sub.id == TestServiceFactory.getAuthServiceMock().getMinimizedLoggedUser().id;
       })
   }

   getSubscriptionOfOtherUser() : Subscribtion {
    return this.getListOfSubscriptions().find((sub : Subscribtion) => {
        return sub.id != TestServiceFactory.getAuthServiceMock().getMinimizedLoggedUser().id;
    }) 
   }
}