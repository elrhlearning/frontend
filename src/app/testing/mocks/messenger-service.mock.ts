import { Message } from '../../modules/shared/entity/message.entity';
import { Conversation } from '../../modules/shared/entity/conversation.entity';
import { TestServiceFactory } from './test-services.factory';
import { User } from '../../modules/shared/entity/user.entity';
export class MockMessengerService {
    private conversations : Conversation[];
    nbNewMessages = 3;
    isSender(message : Message){
      if(message.id == 1 )
        return true;
      return false;
    }


    getAllConversations() : Conversation[]{
      if(!this.conversations){
        this.conversations = [];
        let conv = new Conversation();
        conv.dateCreation = new Date().toISOString();
        conv.id = 1;
        conv.users = [];
        conv.users.push(TestServiceFactory.getUserServiceMock().getConnectedUser());
        conv.users.push(TestServiceFactory.getUserServiceMock().getUser(2));
        conv.countNewMessages = 0;
        conv.messages = [];
        conv.messages.push(this.getMessageWithSenderAndConv(conv.users[0],conv));
        this.conversations.push(conv);
        conv = new Conversation();
        conv.dateCreation = new Date().toISOString();
        conv.id = 2;
        conv.users = [];
        conv.users.push(TestServiceFactory.getUserServiceMock().getConnectedUser());
        conv.users.push(TestServiceFactory.getUserServiceMock().getUser(3));
        conv.countNewMessages = 1;
        conv.messages = [];
        conv.messages.push(this.getMessageWithSenderAndConv(conv.users[0],conv));
        conv.messages.push(this.getMessageWithSenderAndConv(TestServiceFactory.getUserServiceMock().getUser(3),conv));
        this.conversations.push(conv);
      }
      return this.conversations;
    }

    getMessage() : Message {
     let msg = new Message();
     msg.dateCreation = new Date().toISOString();
     msg.id = TestServiceFactory.getRandomInt(1,568);
     msg.read = false;
     msg.content = msg.id.toString();
     return msg;
    }

    getMessageWithSenderAndConv(sender : User,conv : Conversation){
      let m = this.getMessage();
      m.sender = sender;
      m.conversation = conv;
      return m;
    }

    getMessageWithConv(conv : Conversation){
      let m = this.getMessage();
      m.conversation = conv;
      m.sender = conv.users[TestServiceFactory.getRandomInt(0,1)];
      return m;
    }

    loadMessages(conv : Conversation) : Message[]{
      let c = this.conversations.find(cc => cc.id == conv.id);
      if(c){
        if(!c.messages || c.messages.length <=1)
          c.messages = [];
        if(c.messages.length >= 100)
          return [];
        let msgs = [];
        for(let i = 1;i<= 25;i++){
          msgs.push(this.getMessageWithConv(conv));
        }
        conv.messages.concat(msgs);
        return msgs;
      }
      return undefined;
    }
};