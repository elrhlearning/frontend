import { User } from '../../modules/shared/entity/user.entity';
import { Observable, of } from 'rxjs';
import { TestServiceFactory } from './test-services.factory';
export class MockAuthService {
    constructor(){
        if(!this.loggedUser){
            this.loggedUser = TestServiceFactory.getUserServiceMock().getConnectedUser();
        }
    }
//*****************************************************************************************************    
    private loggedUser : User;
    private minimizedLoggedUser : User;
//*****************************************************************************************************
    getMinimizedLoggedUser() : User {
        if(!this.minimizedLoggedUser){
          this.minimizedLoggedUser = new User();
          this.minimizedLoggedUser.id = this.getLoggedUser().id;
          this.minimizedLoggedUser.name = this.getLoggedUser().name;
          this.minimizedLoggedUser.lastname = this.getLoggedUser().lastname;
          this.minimizedLoggedUser.email = this.getLoggedUser().email;
          this.minimizedLoggedUser.phoneNumber = this.getLoggedUser().phoneNumber;
        }
        return this.minimizedLoggedUser;
    }
//*****************************************************************************************************
    getLoggedUser() : User{
        return this.loggedUser;
    } 
//*****************************************************************************************************  
    isLoggedIn() : boolean{
        return this.loggedUser != null;
    }    
//*****************************************************************************************************
    login(user : User) : Observable<boolean> {
        if(!user || !user.password || !user.email)
            return of(false);
        let adm  = TestServiceFactory.getUserServiceMock().getConnectedUser();
        if(user.email==adm.email && user.password==adm.password)
            return of(true);
        return of(false);
    }
//***************************************************************************************************** 
    updateLoggedUserInfos(user : User){
        this.minimizedLoggedUser = undefined;
        this.loggedUser.name = user.name;
        this.loggedUser.lastname = user.lastname;
        this.loggedUser.phoneNumber = user.phoneNumber;
    }  
//***************************************************************************************************** 
    public logout(){
    if(this.getLoggedUser() != null){
      this.loggedUser = undefined;
      this.minimizedLoggedUser = undefined;
      return true;
    }
    return false;
  }  
//*****************************************************************************************************       
}