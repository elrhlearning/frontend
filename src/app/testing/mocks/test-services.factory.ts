import { ReviewServiceMock } from './review-service.mock';
import { MockMessengerService } from './messenger-service.mock';
import { MockAuthService } from './auth-service.mock';
import { MockUserService } from '../../modules/user/services/user-service.mock';
import { MockSubscriptionService } from './subscribtion-service.mock';
export class TestServiceFactory {
 private static reviewsService : ReviewServiceMock;
 private static authService: MockAuthService;
 private static messengerServiceMock : MockMessengerService;
 private static userServiceMock : MockUserService;
 private static subscriptionServiceMock : MockSubscriptionService;

 static getReviewsServiceMock () : ReviewServiceMock {
     if(!this.reviewsService)
        this.reviewsService = new ReviewServiceMock(); 
     return this.reviewsService;
 }  
 
 static getAuthServiceMock () : MockAuthService {
    if(!this.authService)
        this.authService = new MockAuthService();
    return this.authService;
 } 

 static getUserServiceMock () : MockUserService {
    if(!this.userServiceMock)
        this.userServiceMock = new MockUserService();
    return this.userServiceMock;
 } 

 static getMessengerServiceMock() : MockMessengerService {
    if(!this.messengerServiceMock)
        this.messengerServiceMock = new MockMessengerService();
    return this.messengerServiceMock;
 }

 static getSubscriptionServiceMock() : MockSubscriptionService {
    if(!this.subscriptionServiceMock)
        this.subscriptionServiceMock = new MockSubscriptionService();
    return this.subscriptionServiceMock;
 }

 static getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
}