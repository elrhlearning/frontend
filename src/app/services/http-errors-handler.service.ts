import { Injectable, NgZone, Injector, ErrorHandler } from '@angular/core';
import { throwError, Subject } from 'rxjs';
import {HttpErrorResponse } from '@angular/common/http';
import * as HttpStatusCode from 'http-status-codes';
import { MatSnackBar } from '@angular/material';
import { environment } from 'src/environments/environment';
//******************************************************************************************************************
export interface AppRequestErrorResponse{
  error : string;
  statusCode : number;
  message : string;
  data?: any;
  getMessage() : string;
  getDetailedMessage() : string;
}
//******************************************************************************************************************
@Injectable({
  providedIn: 'root'
})
export class HttpErrorHandler implements ErrorHandler{
  public static SnackBarEnabled : boolean = true;
  public static SnackBarDuration : number = 3000;
  private  zone: NgZone;
  private snackBar : MatSnackBar;
  constructor(private injector: Injector) {

  }

  public handleError(error: HttpErrorResponse) {
    let response : AppRequestErrorResponse = undefined;
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
      response = new ClientSideRequestErrorResponse(error.error);
    } else {
      // The backend returned an unsuccessful response code.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
      // prepare repsonse
      switch(error.status){
        case HttpStatusCode.UNAUTHORIZED : 
          response = new UnauthorizedErrorResponse(error);
        break;
        case 0 : 
          response = new RequestFailureErrorResponse(error);
        break;        
        default :
          response = new DefaultAppRequestErrorResponse(error);
        break;
      }
    }
    // show message if snackbar is enabaled (it will show message for every request)
    if(environment.handleErrorsWithSnackbar){
      this.snackBar = this.injector.get(MatSnackBar);
      this.zone = this.injector.get(NgZone);
      this.zone.run(() => {
        this.snackBar.open(response.getMessage(),undefined,{
          duration: HttpErrorHandler.SnackBarDuration,
          horizontalPosition : "center"
       });
      });
    }
    // throw error  
    return throwError(response);
  };
}
//******************************************************************************************************************
/**
 * Default lass to get message from error : HttpErrorResponse when request fail 
 */
export class DefaultAppRequestErrorResponse implements AppRequestErrorResponse{
  error: string;  
  statusCode: number;
  message: string;
  data?: any;

  constructor(error: HttpErrorResponse){
    this.error  = error.statusText,
    this.statusCode =  error.status,
    this.message  = error.error.message,
    this.data = error;
  }

  getDetailedMessage() : string {
    return `Error  : ${this.error}, code : ${this.statusCode}, message : ${this.message}`;
  }

  getMessage() : string {
    return `${this.message}`;
  }
}
//******************************************************************************************************************
/**
 * Default lass to get message from error : HttpErrorResponse when request fail 
 */
export class ClientSideRequestErrorResponse implements AppRequestErrorResponse{
  error: string;  
  statusCode: number;
  message: string;
  data?: any;

  constructor(error: ErrorEvent){
    this.message  = error.message;
    this.data = error;
  }

  getDetailedMessage() : string {
    return `${this.message}`;
  }

  getMessage() : string {
    return `${this.message}`;
  }
}
//******************************************************************************************************************
/**
 * Class to get message from error : HttpErrorResponse when server response is UNAUTHORIZED
 */
export class UnauthorizedErrorResponse extends DefaultAppRequestErrorResponse implements AppRequestErrorResponse{

  getDetailedMessage() : string {
    return `Error  : ${this.error}, code : ${this.statusCode}, message : ${this.message}`;
  }

  getMessage() : string {
    return `Server respond with ${this.error}, message : ${this.message}`;
  }
//******************************************************************************************************************  
}
/**
 * Class to get message from error : HttpErrorResponse when server response is UNAUTHORIZED
 */
export class RequestFailureErrorResponse extends DefaultAppRequestErrorResponse implements AppRequestErrorResponse{

  constructor(error: HttpErrorResponse){
    super(error);
    this.statusCode =  error.status,
    this.message  = error.message,
    this.data = error;
  }

  getDetailedMessage() : string {
    return `Server not responding , code : ${this.statusCode}, message : ${this.message}`;
  }

  getMessage() : string {
    return `Error :  ${this.error}, message : ${this.message}`;
  }
//******************************************************************************************************************  
}