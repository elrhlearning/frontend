import * as io from 'socket.io-client';
import { User } from '../modules/shared/entity/user.entity';
export abstract class WebSocketManager {
  protected socket: io.Socket;
  protected client : User;
  abstract configure() : boolean;

  connect() : any {
    if( this.isSocketReady() && !this.isConnected())
      this.socket.connect();
  }
  setClient(user : User) {
    this.client = user;
  }
  isConnected() : boolean {
    if(!this.isSocketReady())
      return false;
    return this.socket.connected;
  }

  disconnect() : any {
    if(this.isSocketReady())
      this.socket.disconnect();
  }

  isSocketReady() : boolean {
    return this.socket != null || this.socket != undefined;
  }

  getSocket() : io.Socket {
    return this.socket;
  }

  configureAndConnect(){
    // if configure return true it connect socket
    if(!this.configure()){
      this.connect();
    }
  }

  destroy(){
    this.socket.close();
    this.socket = undefined;
  }
}