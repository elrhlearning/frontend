import { WebsocketService } from './websocket.service';
import { HttpErrorHandler } from './http-errors-handler.service';
import { User } from './../modules/shared/entity/user.entity';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { Observable,throwError, Subject, ReplaySubject } from 'rxjs';
import { map,catchError, take, finalize } from 'rxjs/operators';
import * as io from 'socket.io-client';
import { CookieService } from 'ngx-cookie-service';
import { ReplaceSource } from 'webpack-sources';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
//*****************************************************************************************************  
  private cookiesKey : string = 'user';
  private loggedUser : User;
  private minimizedLoggedUser : User;
  socket: io.Socket;
  private getTokenURL = environment.apiURL + 'auth/token';
  public loggedOutUser : Subject<any> = new ReplaySubject(1);
//*****************************************************************************************************  
  constructor(private readonly http: HttpClient,
              private readonly cookieService: CookieService
              ) 
  { 
    this.setSession()
  }
//*****************************************************************************************************
  getMinimizedLoggedUser() : User{
    if(!this.minimizedLoggedUser){
      this.minimizedLoggedUser = new User();
      this.minimizedLoggedUser.id = this.getLoggedUser().id;
      this.minimizedLoggedUser.name = this.getLoggedUser().name;
      this.minimizedLoggedUser.lastname = this.getLoggedUser().lastname;
      this.minimizedLoggedUser.email = this.getLoggedUser().email;
      this.minimizedLoggedUser.phoneNumber = this.getLoggedUser().phoneNumber;
    }
    return this.minimizedLoggedUser;
  }  
//*****************************************************************************************************
  getLoggedUser() : User{
    return this.loggedUser;
  }
//*****************************************************************************************************
  updateLoggedUserInfos(user : User){
    this.minimizedLoggedUser = undefined;
    this.loggedUser.name = user.name;
    this.loggedUser.lastname = user.lastname;
    this.loggedUser.phoneNumber = user.phoneNumber;
    this.cookieService.set(this.cookiesKey,JSON.stringify(this.loggedUser),this.getTokenExpiratrionDate(this.loggedUser.token));
  }  
//*****************************************************************************************************  
  isLoggedIn() : boolean{
    return this.loggedUser != null;
  }
//***************************************************************************************************** 
  /**
   * signin the user (get token from the server)
   * ConcatMap : operator used to use observable value to create another
   * @param user 
   */
  login(user : User) : Observable<boolean> {
    //this.loggedUser = undefined;
    let obs = this.http.post<User>(this.getTokenURL,user);
    return obs.pipe(
      take(1),
      map( (data : User)  => {
        if(this.setSession(data))
          return true;
        return false;
      })
    );
  }
//***************************************************************************************************** 
  public logout(){
    if(this.getLoggedUser() != null){
      this.cookieService.delete(this.cookiesKey);
      this.loggedUser = undefined;
      this.minimizedLoggedUser = undefined;
      this.loggedOutUser.next(true);
      return true;
    }
    return false;
  }    
//***************************************************************************************************** 
  private isValidLocalStorageToken(user : User) : boolean{
    let r : boolean = false;
    try {
      if(!user || !user.token)
        return r;
      // check expiration time
      let date : Date = this.getTokenExpiratrionDate(user.token);
      let now  : Date  = new Date();
      if(date.getTime() > now.getTime()){
        r = true;
      }
      return r; 
    } catch (error) {
      return r;
    }
  }
//*****************************************************************************************************
  private setSession(userParam? : User) : boolean{
    let user : User;
    //get user from localStorage
    if(!userParam){
      // check cookies
      if(this.cookieService.check(this.cookiesKey))
        user = JSON.parse(this.cookieService.get(this.cookiesKey));
      else
        return false;
    }
    else{
      user = userParam;
      this.cookieService.set(this.cookiesKey,JSON.stringify(user),this.getTokenExpiratrionDate(user.token));
    }
    if(this.isValidLocalStorageToken(user)){
      this.loggedUser = user;
      return true;
    }
    return false;
  }
//*****************************************************************************************************
  private getTokenExpiratrionDate(token :any) : Date{
    let date : Date = new Date(token.date);
    let expiresIn = token.expiresIn;
    date.setSeconds(date.getSeconds()  + expiresIn);
    return date;
  }
//*****************************************************************************************************
}