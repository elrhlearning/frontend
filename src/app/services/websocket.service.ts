import { NotificationData } from './../modules/realtime/helpers/notification-data.interface';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { WebSocketManager } from './abstract-websocket.class';
import { NotificationFactoryService } from '../modules/realtime/helpers/notification-factory.service';
@Injectable({
  providedIn: 'root'
})
export class WebsocketService extends WebSocketManager {
//*****************************************************************************************************
  observable:Observable<NotificationData>;
  constructor(private readonly notifFactory : NotificationFactoryService) { 
    super()
  }
//*****************************************************************************************************
  configure(): boolean {
    try {
      if(!this.isSocketReady()){
        this.socket = io.connect(environment.webSocketHost,{
          query: {
            user : JSON.stringify(this.client)
        }});
        this.socket.on('connect', function () {
          console.log('Connected to websocket server');
        });
        
        this.observable = new Observable((observer)=>{ 
          this.getSocket().on('hello',(data)=> {
            observer.next(this.notifFactory.getNotificationData(data.notification));
          });
        });  
      }
      return true; 
    } catch (error) {
      return false;
    }
  }  
  
//*****************************************************************************************************
  onNewNotification() : Observable<NotificationData> {
    return this.observable;
  }
//*****************************************************************************************************
}
