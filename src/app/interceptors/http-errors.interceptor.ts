import { Injectable } from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorHandler } from '../services/http-errors-handler.service';

/** Pass untouched request through to the next request handler. */
@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
//********************************************************************************************* */
  constructor(private httpErrorHandler : HttpErrorHandler){

  }  
//*********************************************************************************************
  intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {
    return next.handle(req)
    .pipe(
        catchError((error) => {
            return this.httpErrorHandler.handleError(error);
        })
    )
  }
  
}