import { Injectable } from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorHandler } from '../services/http-errors-handler.service';
import { AuthService } from '../services/auth.service';
import { User } from '../modules/shared/entity/user.entity';

/** Pass untouched request through to the next request handler. */
@Injectable()
export class HttpTokenInterceptor implements HttpInterceptor {
//********************************************************************************************* */
  constructor(private authService : AuthService){

  }  
//*********************************************************************************************
  intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {
      let user : User = this.authService.getLoggedUser();
      if (user && user.token) {
       req = req.clone({
         setHeaders: {
           Authorization: `Bearer ${user.token.accessToken}`
         }
       });
     }
    return next.handle(req);
  }
  
}