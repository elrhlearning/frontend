import { RealtimeModule } from './modules/realtime/realtime.module';
import { AuthService } from './services/auth.service';
import { WebsocketService } from './services/websocket.service';
import { UserModule } from './modules/user/user.module';
import { SharedModule } from './modules/shared/shared.module';
import { ReviewsModule } from './modules/reviews/reviews.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserMenuComponent } from './components/user-menu/user-menu.component';
import { SidnavContentComponent } from './components/sidnav/sidnav-content.component';
import { DashboardPageComponent } from './pages/dashboard-page/dashboard-page.component';
import { CookieService } from 'ngx-cookie-service';
import { LayoutModule } from '@angular/cdk/layout';
import { MessengerNotificationComponent } from './components/messenger-notification/messenger-notification.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpErrorInterceptor } from './interceptors/http-errors.interceptor';
import { HttpTokenInterceptor } from './interceptors/http-token.interceptor';


@NgModule({
  declarations: [
    AppComponent,UserMenuComponent, SidnavContentComponent,  DashboardPageComponent, MessengerNotificationComponent,
  ],
  imports: [
    // Angular && Angular material && FlexLayout
    BrowserModule,AppRoutingModule,BrowserAnimationsModule,
    //shared
    SharedModule,
    // app modules
    ReviewsModule,
    UserModule,
    LayoutModule,
    RealtimeModule
  ],
  providers: [  
    WebsocketService,
    AuthService,
    CookieService,
    // for interceptor, order is very important, in this case we intercept request (add token)
    { 
      provide: HTTP_INTERCEPTORS, useClass: HttpTokenInterceptor, multi: true 
    },  
    // and we handle error (response)
    { 
      provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true 
    } 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
