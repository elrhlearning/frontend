import { Comment } from './../../../shared/entity/comment.entity';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'comment-form',
  templateUrl: './comment-form.component.html',
  styleUrls: ['./comment-form.component.css']
})
export class CommentFormComponent implements OnInit {

  constructor() { }
  @ViewChild('commentForm') 
  form : NgForm;

  @Input()
  comment : Comment;

  @Output()
  onSubmitComment : EventEmitter<Comment> = new EventEmitter<Comment>();


  ngOnInit() {
    if(!this.comment){
      this.comment = new Comment();
    }
  }

  onSubmit(){
      this.comment.dateCreation = new Date().toISOString();
      this.onSubmitComment.emit(this.comment);
      this.comment = new Comment();
  }

  valideComment(){
    return this.comment && this.comment.content && this.comment.content.length > 0 && (this.comment.content.replace(/\s/g, "")).length > 0
  }
}
