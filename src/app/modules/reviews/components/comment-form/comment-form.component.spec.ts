import { async, ComponentFixture, TestBed,fakeAsync  } from '@angular/core/testing';

import { UserMenuComponent } from '../../../../components/user-menu/user-menu.component';
import { SidnavContentComponent } from '../../../../components/sidnav/sidnav-content.component';
import { DashboardPageComponent } from '../../../../pages/dashboard-page/dashboard-page.component';
import { MessengerNotificationComponent } from '../../../../components/messenger-notification/messenger-notification.component';
import { SharedModule } from '../../../shared/shared.module';
import { By, BrowserModule } from '@angular/platform-browser';
import { ReviewsModule } from '../../reviews.module';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from '../../../../services/auth.service';
import { DebugElement,NO_ERRORS_SCHEMA  } from '@angular/core';
import { TestServiceFactory } from '../../../../testing/mocks/test-services.factory';
import { NotificationComponent } from '../../../realtime/components/notification/notification.component';
import { NotificationMenuComponent } from '../../../realtime/components/notification-menu/notification-menu.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommentFormComponent } from './comment-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Comment } from '../../../shared/entity/comment.entity';

describe('CommentFormComponent', () => {
//************************************************************************************************************************************* 
  let component: CommentFormComponent;
  let fixture: ComponentFixture<CommentFormComponent>;
//*************************************************************************************************************************************
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations : [UserMenuComponent, SidnavContentComponent,  DashboardPageComponent, MessengerNotificationComponent,
        NotificationComponent,NotificationMenuComponent
      ],
      schemas: [ NO_ERRORS_SCHEMA ],
      imports: [ BrowserAnimationsModule,SharedModule,ReviewsModule,RouterTestingModule,FormsModule],
      providers: [
        { provide: AuthService, useFactory : () => { return TestServiceFactory.getAuthServiceMock()} }
      ]
    })
    .compileComponents().then(() => {
      // create component
      fixture = TestBed.createComponent(CommentFormComponent);
      component = fixture.componentInstance;
    })
  }));
//*************************************************************************************************************************************
  it('should create CommentFormComponent and check comment instance', () => {
    fakeAsync(()=> {
      // listen when the component emit
      component.onSubmitComment.subscribe((data : Comment) => {
        expect(data).toBeDefined();
        expect(data.content).toEqual(st);
      });
      // start
      expect(component).toBeTruthy();
      fixture.detectChanges();
      let st = 'Comment value in test';
      // set value to controle
      const ctrl = component.form.controls['content'];
      expect(ctrl).toBeDefined();
      expect(ctrl.value).toBeUndefined();
      ctrl.setValue(st);
      // submit form
      let btn = fixture.debugElement.query(By.css('#btn-add-comment')).nativeElement;
      expect(btn).toBeDefined();
      btn.click();
    });      
  });
//*************************************************************************************************************************************
  it(' CommentFormComponent Should be invalid and submit bouton hidden', () => {
    fakeAsync(()=> {
      // start
      expect(component).toBeTruthy();
      fixture.detectChanges();
      let st = undefined;
      // set value to controle
      const ctrl = component.form.controls['content'];
      expect(ctrl).toBeDefined();
      expect(ctrl.value).toBeUndefined();
      ctrl.setValue(st);
      // form should be invalid
      expect(component.form.valid).toBeFalsy();
      let btn = fixture.debugElement.query(By.css('#btn-add-comment')).nativeElement;
      expect(btn).toBeUndefined();
      // change value
      st ='valid comment';
      ctrl.setValue(st);
      // form should be valid
      expect(component.form.valid).toBeTruthy();
    });      
  });
//*************************************************************************************************************************************
});
