import { Comment } from './../../../shared/entity/comment.entity';
import { IDialogContentBuilder } from '../../../shared/components/delete-dialog-component/dialog-content-builder.interface';
import { Review } from '../../../shared/entity/review.entity';

export class UpdateCommentDialogContentBuilder implements IDialogContentBuilder {
    private comment : Comment;
    btnCancelTitle: string = 'No';
    btnConfirmTitle: string = 'Update';
    constructor(comment : Comment){
        this.comment = comment;
    }
    getMessage() {
        return undefined;
    }

    getDialogTitle(): string {
        return "Update Comment";
    }

    setComment(comment : Comment){
        this.comment = comment;
    }

    getObject() : Comment {
        return this.comment
    }
}