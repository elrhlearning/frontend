import { LoadingCommanderService } from './../../../shared/services/loading-commander.service';
import { ReviewService } from './../../services/review.service';
import { UpdateCommentDialogContentBuilder } from './update-comment-dialog-content.builder';
import { Comment } from './../../../shared/entity/comment.entity';
import { IDialogContentBuilder } from './../../../shared/components/delete-dialog-component/dialog-content-builder.interface';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-dialog-update-comment',
  templateUrl: './dialog-update-comment.component.html',
  styleUrls: ['./dialog-update-comment.component.css']
})
export class DialogUpdateCommentComponent implements OnInit {
  comment : Comment;
  constructor(  public dialogRef: MatDialogRef<DialogUpdateCommentComponent>,
                private  loader : LoadingCommanderService,
                private readonly reviewService : ReviewService,
                @Inject(MAT_DIALOG_DATA) public data: IDialogContentBuilder) {

  }
//*********************************************************************************************************
  ngOnInit() {
    this.comment = (<UpdateCommentDialogContentBuilder>this.data).getObject();
    this.comment = JSON.parse(JSON.stringify(this.comment));
  }
//*********************************************************************************************************
  updateComment(event) {
    this.loader.startLoading();
    if(this.comment.content != (<UpdateCommentDialogContentBuilder>this.data).getObject().content)
      this.reviewService.updateComment(this.comment)
      .subscribe(
        (data : boolean)=>{
          if(data){
            (<UpdateCommentDialogContentBuilder>this.data).getObject().content = this.comment.content;
          }  
        },
        (error) => {},
        () => {
          this.loader.stopLoading();
          this.dialogRef.close();
        }
      );
  }
//*********************************************************************************************************
}
