import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { MessengerService } from '../../../realtime/services/messenger.service';
import { Conversation } from '../../../shared/entity/conversation.entity';
import { environment } from './../../../../../environments/environment';
import { SubscribtionService } from './../../../realtime/services/subscribtion.service';
import { ConfirmationDialogComponent } from './../../../shared/components/delete-dialog-component/confirmation-dialog.component';
import { IDialogContentBuilder } from './../../../shared/components/delete-dialog-component/dialog-content-builder.interface';
import { Review } from './../../../shared/entity/review.entity';
import { Subscribtion } from './../../../shared/entity/subscribtion.entity';
import { LoadingCommanderService } from './../../../shared/services/loading-commander.service';
import { ReviewService } from './../../services/review.service';
import { DeleteReviewDialogContentBuilder } from './delete-review-dialog-content.builder';
declare var window: any;

@Component({
  selector: 'review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {
//*********************************************************************************************************
  @Input()
  review : Review ;
  @Output()
  onDeleteReview: EventEmitter<Review> = new EventEmitter();
  @Output()
  onUpdateReviewRequest : EventEmitter<Review> = new EventEmitter();
  @Output()
  onGoToConversationRequest : EventEmitter<Conversation> = new EventEmitter();
  private imgContent : any;
//*********************************************************************************************************  
  isReviewOfLoggedUser : boolean = false;
  private deleteReviewContentDialogBuilder : IDialogContentBuilder; 
  assetsPath = environment.assetsPath;
  subscription : Subscribtion = null;
//*********************************************************************************************************
  constructor(
    public readonly reviewService : ReviewService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private loadingCommander : LoadingCommanderService,
    public readonly subscribService : SubscribtionService,
    private readonly messengerService : MessengerService
    ) { 
    this.deleteReviewContentDialogBuilder = new DeleteReviewDialogContentBuilder(this.review);
  }
//*********************************************************************************************************
   ngOnInit() {
    (<DeleteReviewDialogContentBuilder>this.deleteReviewContentDialogBuilder).setReview(this.review);
    this.isReviewOfLoggedUser = this.reviewService.isReviewOfLoggedUser(this.review);
    this.subscribService.isSubscribedToReview(this.review)
    .subscribe(
      (data : Subscribtion) => {
        this.subscription  = data;
      }
    );
  }
//*********************************************************************************************************
  openDeleteDialog(): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '50%',
      minWidth : '300px',
      maxWidth : '600px',
      closeOnNavigation: false,
      data: this.deleteReviewContentDialogBuilder
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result == true){
        this.deleteReview();
      }
    });
  }
//*********************************************************************************************************
  private deleteReview(){ 
    this.loadingCommander.startLoading();
    this.reviewService.deleteReview(this.review)
    .subscribe(
      (data : any)=>{
        setTimeout(() => {
          this.loadingCommander.stopLoading();
          this.onDeleteReview.emit(this.review);
        }, 900);
      },
      (error) => {}
    );    
  }
//*********************************************************************************************************
  public subscribe() {
     this.subscribService.startSubscribtion(this.review)
     .subscribe(
      (data : Subscribtion) => {
        this.subscription = data;
        this.snackBar.open(`Subscribting to Review : ${this.subscription.review.title}`,undefined,{
          duration: 1500,
          horizontalPosition : "center"
       });
      }
     );
  }
//*********************************************************************************************************  
  public unsubscribe(){
    this.subscribService.stopSubscription(this.subscription)
    .subscribe(
     (data : boolean) => {
      if(data){
        this.snackBar.open(`Unsubscribting from review : ${this.subscription.review.id}`,undefined,{
          duration: 1500,
          horizontalPosition : "center"
       });
       this.subscription = null;
      }
     }
    );
  }
//*********************************************************************************************************
  public sendUpdateReviewRequest(){
    this.onUpdateReviewRequest.emit(this.review);
  }
//*********************************************************************************************************
  public goToConversation(){
    this.messengerService.getConversation(this.review.user)
    .subscribe(
      (data : Conversation) => {
        this.onGoToConversationRequest.emit(data);
      }
    );
  }
//*********************************************************************************************************
  getImageSrc() : any{
    if (this.review.image && this.review.image.name){
      return environment.assetsPath + 'images/'+ this.review.image.name;
    }
  }
//*****************************************************************************************************
  getURL(){
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = `${window.location.host}/review/${this.review.id}`;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.snackBar.open(`Link to  : ${this.review.title} is copied to clipboard`,undefined,{
      duration: 1500,
      horizontalPosition : "center"
   });
  }
//*****************************************************************************************************
}
