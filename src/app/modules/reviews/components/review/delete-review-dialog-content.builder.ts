import { IDialogContentBuilder } from '../../../shared/components/delete-dialog-component/dialog-content-builder.interface';
import { Review } from '../../../shared/entity/review.entity';

export class DeleteReviewDialogContentBuilder implements IDialogContentBuilder {
    private review : Review;
    btnCancelTitle: string = 'No';
    btnConfirmTitle: string = 'Delete';
    constructor(review : Review){
        this.review = review;
    }
    getMessage() {
        return `Are you Sure to delete review : '${this.review.title}' !`;
    }

    getDialogTitle(): string {
        return "Delete Review";
    }

    setReview(review : Review){
        this.review =review;
    }

    getObject() {
        return undefined;
    }
}