/**
 * Title metadata
 */
const title  = {
  label : 'Title',
  minlength : 5,
}
const titleErrors = {
  required : 'Title is required.',
  minlength : `Title must contains ${title.minlength}!`
}
//*****************************************************************
/**
 * Link metadata
 */
const link  = {
  label : 'Link',
  minlength : 3,
}
const linkErrors = {
  required : 'Link is required.',
  isLink : 'Invalid URL Please Check the Link',
  minlength : `Link must contains ${link.minlength}!`
}
//*****************************************************************
/**
 * Description metadata
 */
const desc  = {
  label : 'Description',
  minlength : 5,
  maxlength : 1500,
}
const descErrors = {
  required : 'Description is required.',
  minlength : `Description must contains ${desc.minlength}!`,
  maxlength : `Description must contains max ${desc.maxlength}!`
}
//*****************************************************************
/**
 * Key metadata
 */
const key = {
  label : 'Title :',
  minlength : 20,
  maxlength : 200,
}
const keyErrors = {
  required : 'Title is required.',
  minlength : `Title must contains ${key.minlength}!`,
  maxlength : `Title must contains max ${key.maxlength}!`
}
//*****************************************************************
/**
 * Value metadata
 */
const value = {
  label : 'Paragraph :',
  minlength : 20,
  maxlength : 2000,
}
const valueErrors = {
  required : 'Paragraph is required.',
  minlength : `Paragraph must contains ${value.minlength}!`,
  maxlength : `Paragraph must contains max ${value.maxlength}!`
}
//*****************************************************************
const fields = {
    // title
    title : {
      label : title.label,
      minlength : title.minlength,
      errors : titleErrors
    },
    // link
    link : {
      label : link.label,
      minlength : link.minlength,
      errors : linkErrors
    },
    // desc
    desc : {
      label : desc.label,
      minlength : desc.minlength,
      maxlength : desc.maxlength,
      errors : descErrors
    },
    // key 
    key : {
      label : key.label,
      minlength : key.minlength,
      maxlength : key.maxlength,
      errors : keyErrors
    },
    // value
    value : {
      label : value.label,
      minlength : value.minlength,
      maxlength : value.maxlength,
      errors : valueErrors
    }
}
/**
 * Form metadata
 */
export const FORM_REVIEW_METADATA : any = {
    forUpdate : {
      btnTitle : 'Save',
      btnAddKey : 'Add item',
      btnDeleteAllKeys : 'Delete All',
      fields  : fields
    },
    forCreate : {
      btnTitle : 'Add',
      btnAddKey : 'Add item',
      btnDeleteAllKeys : 'Delete All',
      fields  : fields
    },
}