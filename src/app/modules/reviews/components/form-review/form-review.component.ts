import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Image } from 'src/app/modules/shared/entity/image.entity';
import { environment } from './../../../../../environments/environment';
import { ItemReview } from './../../../shared/entity/item-review.entity';
import { Review } from './../../../shared/entity/review.entity';
import { FORM_REVIEW_METADATA } from './form-review.metadata';
@Component({
  selector: 'form-review',
  templateUrl: './form-review.component.html',
  styleUrls: ['./form-review.component.css']
})
export class FormReviewComponent implements OnInit {
//*****************************************************************************************************
  /**
   * Input Review Object
   */
  @Input()
  review : Review;
  /**
   * eventEmitter used to send the Review Object
   */
  @Output()
  private onSubmitReview : EventEmitter<Review> = new EventEmitter();

  private imgContent : any;

  @Input()
  forUpdate : boolean = false;

  @Input()
  imageReady : boolean;

  formMetadata : any;
//*****************************************************************************************************  
  constructor() {
    if(!this.review)
      this.review = new Review();
    if(!this.review.image)
      this.review.image = new Image();
  }
//*****************************************************************************************************
  ngOnInit() {
    this.formMetadata = FORM_REVIEW_METADATA.forCreate;
    if(this.forUpdate){
      this.formMetadata = FORM_REVIEW_METADATA.forUpdate;
    }
  }
//*****************************************************************************************************
  indexTracker(index: number, value: ItemReview) {
    return index;
  }
  addItem(){
    if(!this.review.items)
      this.review.items = [];  
    this.review.items.push(new ItemReview());
  }
//*****************************************************************************************************
  deleteItem(item : ItemReview){
    var index = this.review.items.indexOf(item);
    if (index !== -1) this.review.items.splice(index, 1);
  }
//*****************************************************************************************************
  deleteAll() {
    this.review.items = undefined;
    this.review.items = [];
  }
//*****************************************************************************************************  
  onSubmit(){
    this.onSubmitReview.emit(this.review);
  }
//*****************************************************************************************************  
  onFileInput(files : FileList){
    if(!this.review.image)
      this.review.image = new Image();
    this.review.image.file = files.item(0);
    if(this.review.image.file){
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.imgContent = e.target.result;
      };
      reader.readAsDataURL(files.item(0));
      this.imageReady = true;
    }
  }
//*****************************************************************************************************  
  isImageReady() : boolean{
    if(!this.review || !this.review.image)
      return false;
    return (this.review.image.file != undefined) || (this.review.image.name != undefined && this.review.image.id != undefined);
  }
//*****************************************************************************************************  
  getImageSrc() : any{
    if(this.review.image.file){
      return this.imgContent;
    }
    else if (this.review.image.name){
      return environment.assetsPath + 'images/'+ this.review.image.name;
    }
  }
//*****************************************************************************************************  
}
