import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AuthService } from './../../../../services/auth.service';
import { Comment } from './../../../shared/entity/comment.entity';

@Component({
  selector: 'comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {
//*********************************************************************************************************  
  @Input()
  comment : Comment;

  @Output()
  onDeleteComment : EventEmitter<Comment> = new EventEmitter();

  @Output()
  onUpdateComment : EventEmitter<Comment> = new EventEmitter();
//*********************************************************************************************************
  constructor(private authService : AuthService) { }
//*********************************************************************************************************
  ngOnInit() {
  }
//*********************************************************************************************************
  isCommentOwner(){
    if(!this.comment.owner)
      return false;
    if(this.authService.getMinimizedLoggedUser().id == this.comment.owner.id)
      return true;
    return false;
  }
//*********************************************************************************************************
  sendCommentToDelete(){
    this.onDeleteComment.emit(this.comment);
  }
//*********************************************************************************************************
  sendCommentToUpdate(){
    this.onUpdateComment.emit(this.comment);
  }
//*********************************************************************************************************  
}
