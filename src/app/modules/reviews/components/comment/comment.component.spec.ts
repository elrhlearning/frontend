import { async, ComponentFixture, TestBed,  } from '@angular/core/testing';

import { CommentComponent } from './comment.component';
import { UserMenuComponent } from '../../../../components/user-menu/user-menu.component';
import { SidnavContentComponent } from '../../../../components/sidnav/sidnav-content.component';
import { DashboardPageComponent } from '../../../../pages/dashboard-page/dashboard-page.component';
import { MessengerNotificationComponent } from '../../../../components/messenger-notification/messenger-notification.component';
import { SharedModule } from '../../../shared/shared.module';
import { By } from '@angular/platform-browser';
import { ReviewsModule } from '../../reviews.module';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from '../../../../services/auth.service';
import { DebugElement,NO_ERRORS_SCHEMA  } from '@angular/core';
import { TestServiceFactory } from '../../../../testing/mocks/test-services.factory';
import { NotificationComponent } from '../../../realtime/components/notification/notification.component';
import { NotificationMenuComponent } from '../../../realtime/components/notification-menu/notification-menu.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


describe('CommentComponent', () => {
  let component: CommentComponent;
  let fixture: ComponentFixture<CommentComponent>;
  let comment : Comment;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations : [UserMenuComponent, SidnavContentComponent,  DashboardPageComponent, MessengerNotificationComponent,
        NotificationComponent,NotificationMenuComponent
      ],
      schemas: [ NO_ERRORS_SCHEMA ],
      imports: [ BrowserAnimationsModule,SharedModule,ReviewsModule,RouterTestingModule],
      providers: [
        { provide: AuthService, useFactory : () => { return TestServiceFactory.getAuthServiceMock()} }
      ]
    })
    .compileComponents();
  }
  ));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentComponent);
    component = fixture.componentInstance;
    component.comment = TestServiceFactory.getReviewsServiceMock().getRandomComment();
    component.comment.owner = TestServiceFactory.getUserServiceMock().getConnectedUser();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  
  it('should show menu to edit comment', () => {
    fixture.detectChanges();
    expect(component.isCommentOwner()).toBeTruthy();
    let settingBtn = fixture.debugElement.query(By.css('.div-comment-setting'));
    expect(settingBtn.nativeElement).toBeDefined();
    let btn = fixture.debugElement.query(By.css('.btn-show-comment-menu')).nativeElement;
    expect(btn).toBeDefined();
    btn.click();
    fixture.detectChanges();
    let menu = fixture.debugElement.query(By.css('.comment-menu')).nativeElement;
    expect(menu).toBeDefined();
  });

  
  it('should not show menu to edit comment', () => {
    component.comment.owner = TestServiceFactory.getUserServiceMock().getUser(2);
    fixture.detectChanges();
    expect(component.isCommentOwner()).toBeFalsy();
    let settingBtn = fixture.debugElement.query(By.css('.div-comment-setting'));
    expect(settingBtn).toBeNull();
  });
});
