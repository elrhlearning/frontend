import { DialogUpdateCommentComponent } from './../reviews/components/dialog-update-comment/dialog-update-comment.component';
import { ReviewService } from './services/review.service';
import { ReviewDetailsComponent } from './../reviews/pages/review-details/review-details.component';
import { SharedModule } from './../shared/shared.module';
import { ReviewComponent } from 'src/app/modules/reviews/components/review/review.component';
import { AllReviewsComponent } from 'src/app/modules/reviews/pages/all-reviews/all-reviews.component';
import { NgModule } from '@angular/core';
//import { ReviewsRoutingModule } from './reviews-routing.module';
import { ReviewEntryComponent } from './review-entry.component';
import { FormReviewComponent } from './components/form-review/form-review.component';
import { AddReviewComponent } from './pages/add-review/add-review.component';
import {  FormsModule } from '@angular/forms';
import { UpdateReviewPageComponent } from './pages/update-review-page/update-review-page.component';
import { CommentComponent } from './components/comment/comment.component';
import { CommentFormComponent } from './components/comment-form/comment-form.component';
import { ReviewsRoutingModule } from './reviews-routing.module';



@NgModule({
  declarations: [
    ReviewEntryComponent, AllReviewsComponent, ReviewComponent, 
    DialogUpdateCommentComponent,
    ReviewDetailsComponent, FormReviewComponent, AddReviewComponent, UpdateReviewPageComponent, CommentComponent, CommentFormComponent],
  imports: [
    FormsModule,
    // shared
    SharedModule,
    // routing
    ReviewsRoutingModule,
  ],
  providers : [
    ReviewService,
  ],
  entryComponents: [ReviewEntryComponent,DialogUpdateCommentComponent]
})
export class ReviewsModule { }
