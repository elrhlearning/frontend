import { Router } from '@angular/router';
import { SubscribtionService } from './../../../realtime/services/subscribtion.service';
import { ReviewService, ReviewSearchOrders, ReviewSearchOrderItems, OrderBy } from './../../services/review.service';
import { Observable, Subject, of, } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';
import { Review } from './../../../shared/entity/review.entity';
import { Component, OnInit } from '@angular/core';
import {plainToClass} from "class-transformer";
import { Conversation } from '../../../shared/entity/conversation.entity';
import { GetReviewsParams } from '../../services/review.service';
import { LoadingCommanderService } from '../../../shared/services/loading-commander.service';

@Component({
  selector: 'all-reviews',
  templateUrl: './all-reviews.component.html',
  styleUrls: ['./all-reviews.component.css']
})
export class AllReviewsComponent implements OnInit {
//*********************************************************************************************************  
  reviews$ : Observable<Review[]>;
  reviews : Review[];
  reviewParamsSearchSubject$ = new Subject<GetReviewsParams>();
  reviewParamsLoadingSubject$ = new Subject<GetReviewsParams>();
  reviewParams : GetReviewsParams = {};
  selectedOrder : OrderBy;
  reviewOrderItems : OrderBy[];
  constructor(private readonly reviewService : ReviewService,
              private readonly router : Router,
              private loadingService : LoadingCommanderService,
              private readonly subscribService : SubscribtionService) { 
                this.reviewOrderItems = ReviewSearchOrderItems;
                this.selectedOrder = this.reviewOrderItems[0];
                this.reviewParams.orderBy = [this.selectedOrder];
              }
//*********************************************************************************************************
  ngOnInit() {
    this.loadingService.startLoading();
    this.reviewParams = {skip : 0,take : 15};
    this.subscribService.initLoggedUserSubscribtion()
    .pipe(
      switchMap(()  => { 
          return this.reviewService.getAll(this.reviewParams);
      }),
      catchError(error => {this.loadingService.stopLoading(); return of(error);})
    )
    .subscribe(
      (data : Review[]) => {
        this.reviews = [];
        this.handleResult(data);
        this.loadingService.stopLoading();
      }
    ) 
    //search
    this.reviewService.search(this.reviewParamsSearchSubject$)
    .subscribe(
      (data : Review[])=> {
        this.reviews = [];
        this.handleResult(data);
        this.loadingService.stopLoading();
      }
    );
    // loading
    this.reviewService.load(this.reviewParamsLoadingSubject$)
    .subscribe(
      (data : Review[])=> {
        this.handleResult(data);
        this.loadingService.stopLoading();
      }
    )
  }
//*********************************************************************************************************  
  updateListeReviews(review : Review){
    this.reviews = this.reviews.filter(function(value : Review){
      return value.id != review.id;
    });
    review = null;
  }
//*********************************************************************************************************  
  goToUpdateReviewPage(review : Review){
    this.router.navigate([`/review/update/${review.id}`]);
  }
//*********************************************************************************************************  
  goToConversation(conversation : Conversation){
    this.router.navigate([`/messenger`],{queryParams : {c : conversation.id}});
  }
//*********************************************************************************************************  
  private handleResult(data : Review[]) {
    if(data && data.length > 0)
    data.forEach((review : Review)=>{
      this.reviews.push(plainToClass(Review,review));
    });
  }
//*********************************************************************************************************
  handleKeyup(searchKeyword : string) {
    this.reviewParams.keywordSearch = searchKeyword;
    let paramCopy = JSON.parse(JSON.stringify(this.reviewParams));
    paramCopy.keywordSearch = searchKeyword;
    this.reviewParams.skip = 0;
    this.loadingService.startLoading();
    this.reviewParamsSearchSubject$.next(paramCopy);
    paramCopy = undefined;
  }
//*********************************************************************************************************  
  handleListReviewsScrolling(event): void {  
    if((event.target.scrollTop +event.target.offsetHeight)  == event.target.scrollHeight){
      this.reviewParams.skip = this.reviewParams.skip + 30;
      this.loadingService.startLoading();
      this.reviewParamsLoadingSubject$.next(this.reviewParams);
    }
  }
//*********************************************************************************************************  
  setOrder(order : OrderBy){
    this.loadingService.startLoading();
    this.reviewParams.orderBy = [order];
    let paramCopy = JSON.parse(JSON.stringify(this.reviewParams));
    this.reviewParamsSearchSubject$.next(paramCopy);
  }
//*********************************************************************************************************
}
