import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { ReviewService } from '../../services/review.service';
import { environment } from './../../../../../environments/environment';
import { DialogUpdateCommentComponent } from './../../../reviews/components/dialog-update-comment/dialog-update-comment.component';
import { UpdateCommentDialogContentBuilder } from './../../../reviews/components/dialog-update-comment/update-comment-dialog-content.builder';
import { Comment } from './../../../shared/entity/comment.entity';
import { Review } from './../../../shared/entity/review.entity';
import { LoadingCommanderService } from './../../../shared/services/loading-commander.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'review-details',
  templateUrl: './review-details.component.html',
  styleUrls: ['./review-details.component.css']
})
export class ReviewDetailsComponent implements OnInit {
  review : Review;
  private dialogContent : UpdateCommentDialogContentBuilder;
  assetsPath = environment.assetsPath;
  constructor(private localtion : Location,
              public dialog: MatDialog,
              private  loader : LoadingCommanderService,
              private reviewService : ReviewService,
              private route: ActivatedRoute,) { }
//*********************************************************************************************************
  ngOnInit(  ) {
    this.route.data
      .subscribe((data: { review: Review }) => {
        this.review = data.review;
      });
  }
//*********************************************************************************************************
  goBack(){
      this.localtion.back();
  }
//*********************************************************************************************************
  addComment(comment : Comment){
    this.loader.startLoading();
    if(!this.review.comments)
      this.review.comments = [];
    // set review
    comment.review = this.reviewService.getMinimizedReview(this.review);
    // call service
    this.reviewService.addComment(comment)
    .subscribe(
      (data : Comment)=> {
        data.dateCreation = comment.dateCreation;
        this.review.comments.unshift(data);
        this.loader.stopLoading();
      }
    );
  }
//*********************************************************************************************************  
  updateComment(comment : Comment){
    if(!comment.review){
      comment.review = this.reviewService.getMinimizedReview(this.review);
    }
    if(!this.dialogContent)
      this.dialogContent = new UpdateCommentDialogContentBuilder(comment);
    else  
      this.dialogContent.setComment(comment);

    const dialogRef = this.dialog.open(DialogUpdateCommentComponent, {
      width: '650px',
      data: this.dialogContent
    });

    dialogRef.afterClosed().subscribe(
      (result : Comment) => {
      },
      (err) => {},
      () => {}
    );
  }
//*********************************************************************************************************
  deleteComment(comment : Comment) {
    this.loader.startLoading();
    this.reviewService.deleteComment(comment)
    .subscribe(
      (data : boolean)=>{
        if(data){
          this.review.comments = this.review.comments.filter((c) => {
            return c.id != comment.id;
          });
        }
      },
      (err) => {},
      () => {this.loader.stopLoading();}
    )
  }
//*********************************************************************************************************
 public refresh(){
  this.loader.startLoading();
  this.reviewService.getReview(this.review.id).pipe(take(1))
  .subscribe(
    (data : Review) => {
      this.review = data;
    },
    (error) => {},
    () => {this.loader.stopLoading();}
  )
 }
//*********************************************************************************************************  
}
