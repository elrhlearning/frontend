import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { AuthService } from './../../../../services/auth.service';
import { Review } from './../../../shared/entity/review.entity';
import { LoadingCommanderService } from './../../../shared/services/loading-commander.service';
import { ReviewService } from './../../services/review.service';

@Component({
  selector: 'add-review-page',
  templateUrl: './add-review.component.html',
  styleUrls: ['./add-review.component.css']
})
export class AddReviewComponent implements OnInit {

  constructor(private loadingCommander : LoadingCommanderService,
              private reviewService : ReviewService,
              private snackBar: MatSnackBar,
              private router : Router,
              private authService : AuthService) { }
  
  imageReady : boolean =false;            
  ngOnInit() {
  }


  addReview(review : Review){
    // start loading
    this.loadingCommander.startLoading();
    // set logged suer
    review.user = this.authService.getMinimizedLoggedUser();
    // add review
    this.reviewService.addReview(review)
    .subscribe((data : Review )=>{
      review = data;
    },
    (error) => {
      setTimeout(() => {
        this.router.navigate(['/review']);
       }, 1500);
    },
    ()=> {
      this.loadingCommander.stopLoading();
      if(review.image.id != null){
        this.snackBar.open(`Review  : ${review.link} is created`,undefined,{
          duration: 1500,
          horizontalPosition : "center"
       });
       setTimeout(() => {
        this.router.navigate(['/review']);
       }, 1500);
      }
    }
    );
  }
}
