import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateReviewPageComponent } from './update-review-page.component';

describe('UpdateReviewPageComponent', () => {
  let component: UpdateReviewPageComponent;
  let fixture: ComponentFixture<UpdateReviewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateReviewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateReviewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
