import { ActivatedRoute } from '@angular/router';
import { Review } from './../../../shared/entity/review.entity';
import { Component, OnInit } from '@angular/core';
import { ReviewService } from '../../services/review.service';
import { LoadingCommanderService } from '../../../shared/services/loading-commander.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-update-review-page',
  templateUrl: './update-review-page.component.html',
  styleUrls: ['./update-review-page.component.css']
})
export class UpdateReviewPageComponent implements OnInit {
  review : Review;
  constructor(private route: ActivatedRoute,
              private readonly loadingCommander : LoadingCommanderService, 
              private snackBar: MatSnackBar,
              private readonly reviewService : ReviewService) { }

  ngOnInit(  ) {
    this.route.data
      .subscribe((data: { review: Review }) => {
        this.review = data.review;
      });
  }

  updateReview(review : Review){
    this.loadingCommander.startLoading();
    this.reviewService.updateReview(review)
    .subscribe(
      (data : boolean) => {
        if(data)
        this.snackBar.open(`Review  : ${review.title} updated correctly`,undefined,{
          duration: 1500,
          horizontalPosition : "center"
       });
      },
      (error) => {
        
      },
      ()=> {
        this.loadingCommander.stopLoading();
      }
    )

  }
}
