import { UpdateReviewPageComponent } from './pages/update-review-page/update-review-page.component';
import { AuthGuard } from './../../guards/auth.guard';
import { DashboardPageComponent } from './../../pages/dashboard-page/dashboard-page.component';
import { ReviewResolverService } from './resolvers/review-resolver.service';
import { AddReviewComponent } from './pages/add-review/add-review.component';
import { ReviewDetailsComponent } from 'src/app/modules/reviews/pages/review-details/review-details.component';
import { AllReviewsComponent } from 'src/app/modules/reviews/pages/all-reviews/all-reviews.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReviewEntryComponent } from './review-entry.component';

export const reviewModuleRoutes: Routes = [
  {
    path : '', 
    component : AllReviewsComponent,
    pathMatch: 'full'
  },
  {
    path : 'add', 
    component : AddReviewComponent,
    pathMatch: 'full'
  },
  {
    path : ':id', 
    component : ReviewDetailsComponent,
    pathMatch: 'full',
    resolve : {
      review : ReviewResolverService
    }
  },
  {
    path : 'update/:id', 
    component : UpdateReviewPageComponent,
    pathMatch: 'full',
    resolve : {
      review : ReviewResolverService
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild([])],
  exports: [RouterModule]
})
export class ReviewsRoutingModule { } 
