import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'reviews-entry-component',
  template: `
      <router-outlet></router-outlet>
  `,
  styles : [
   `
   :host{
    height: 100%;
    overflow-y : auto;
    margin : 0;
    padding : 0;
  }
   `
  ]
})
export class ReviewEntryComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
