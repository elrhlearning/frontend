import { ReviewService } from './../services/review.service';
import { Review } from './../../shared/entity/review.entity';
import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
}                                 from '@angular/router';
import { Observable, of, EMPTY }  from 'rxjs';
import { mergeMap, take }         from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReviewResolverService implements Resolve<Review> {

  constructor(private router: Router,private reviewService : ReviewService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Review | Observable<Review> | Promise<Review> {
    let id = route.paramMap.get('id');

     return this.reviewService.getReview(id).pipe(
      take(1),
      mergeMap(review => {
        if (review) {
          return of(review);
        } else { // id not found
          this.router.navigate(['/review']);
          return EMPTY;
        }
      })
    );
  }

}
