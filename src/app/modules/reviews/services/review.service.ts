import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, catchError } from 'rxjs/operators';
import { environment } from './../../../../environments/environment';
import { AuthService } from './../../../services/auth.service';
import { Comment } from './../../shared/entity/comment.entity';
import { Review } from './../../shared/entity/review.entity';
import { User } from './../../shared/entity/user.entity';
import { HttpErrorHandler } from 'src/app/services/http-errors-handler.service';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {
//*******************************************************************************************
  private addReviewURL = environment.apiURL + 'review/add';
  private allReviewURL = environment.apiURL + 'review/all';
  private deleteReviewURL = environment.apiURL + 'review';
  private getReviewByIdURL = environment.apiURL + 'review';
  private addCommentURL = environment.apiURL + 'review/comment/add';
  private updateReviewURL = environment.apiURL + 'review/update';
  private updateCommentURL = environment.apiURL + 'review/comment/update';
  private deleteCommentURL = environment.apiURL + 'review/comment';
  constructor(private http: HttpClient,
              private httpErrorHandler : HttpErrorHandler,
              private readonly authService : AuthService) { }
//*******************************************************************************************
  /**
   * 
   * @param review add new via POST request
   */
  public addReview(review : Review) : Observable<Review>{
    var headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    let formData = new FormData();
    let file =  review.image.file;
    review.image = undefined;
    formData.append('image', file);
    formData.append('review', JSON.stringify(review));
    return this.http.post<Review>(this.addReviewURL,formData,{headers : headers});
  }
//*******************************************************************************************
  public updateReview(review : Review) : Observable<boolean> {
    var headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    let formData = new FormData();
    if(review.image.file){
      let file =  review.image.file;
      formData.append('image', file);
    }
    let reviewCopy = <Review>JSON.parse(JSON.stringify(review));
    reviewCopy.comments = undefined;
    reviewCopy.dateCreation=undefined;
    reviewCopy.user=undefined;
    reviewCopy.image=undefined;
    formData.append('review', JSON.stringify(reviewCopy));
    return this.http.post<boolean>(this.updateReviewURL,formData,{headers : headers});
  }
//*******************************************************************************************
  public getAll(params? : GetReviewsParams) : Observable<Review[]>{
    return this.http.post<Review[]>(this.allReviewURL,params);
  }
//*******************************************************************************************
  public search(params: Observable<GetReviewsParams>) : Observable<Review[]> {
    return params.pipe(
    // wait 600ms before emitting value  
    debounceTime(400),
    // emit only if value is different from previous value
    distinctUntilChanged(),
    switchMap((p : GetReviewsParams) => {
      return this.getAll(p);
    }));
  }
//*******************************************************************************************
public load(params: Observable<GetReviewsParams>) : Observable<Review[]> {
  return params.pipe(
  switchMap((p : GetReviewsParams) => {
    return this.getAll(p);
  })
  );
}  
//*******************************************************************************************
  public getReview(id : any) : Observable<Review>{
    return this.http.get<Review>(`${this.getReviewByIdURL}/${id}`);
  }
//*******************************************************************************************
  public deleteReview(review : Review) : Observable<boolean>{
    return this.http.delete<boolean>(`${this.deleteReviewURL}/${review.id}`);
  }
//*******************************************************************************************
  public isReviewOfLoggedUser(review : Review){
    let user : User = this.authService.getMinimizedLoggedUser();
    if(!user ||  !review || !review.user || !review.user.id || !user.id)
      return false;
    return user.id == review.user.id;
  }
//*******************************************************************************************
  public addComment(comment : Comment) : Observable<Comment>{
    comment.owner = this.authService.getMinimizedLoggedUser();
    return this.http.post<Comment>(this.addCommentURL,comment);
  }  
//*******************************************************************************************
  public deleteComment(comment : Comment) : Observable<boolean> {
    comment.owner = this.authService.getMinimizedLoggedUser();
    return this.http.delete<boolean>(`${this.deleteCommentURL}/${comment.id}`);
  }  
//*******************************************************************************************  
  public updateComment(comment : Comment) : Observable<boolean> {
    return this.http.post<boolean>(this.updateCommentURL,comment);
  }  
//******************************************************************************************* 
  getMinimizedReview(review  : Review) : Review {
    if(review){
      let r = new Review();
      r.id = review.id;
      r.title = review.title;
      r.link = review.link;
      return r;
    }
    return review;
  }
//*******************************************************************************************  
}

export declare type ReviewSearchOrders = "review.link" | "review.title" | "review.description" | "review.dateCreation";

export const ReviewSearchOrderItems : OrderBy[] = [
  {
    column : "review.dateCreation",
    label : "Creation Date",
    order : "DESC",
  },
  {
    column : "review.link",
    label : "Link",
    order : "ASC",
  },
  {
    column : "review.description",
    label : "Description",
    order : "ASC",
  },
  {
    column : "review.title",
    label : "Title",
    order : "ASC",
  }
]

export interface Where {
  where : string;
  params? : any;
}

export interface OrderBy {
  column : ReviewSearchOrders;
  label : string;
  order? : "ASC" | "DESC";
}
export interface GetReviewsParams {
  wheres? : Where[];
  skip? : number;
  take?: number;
  orderBy ?: OrderBy[],
  keywordSearch ? : string;
}
