import { Review } from './../../shared/entity/review.entity';
import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
}                                 from '@angular/router';
import { Observable, of, EMPTY }  from 'rxjs';
import { mergeMap, take }         from 'rxjs/operators';
import { Conversation } from '../../shared/entity/conversation.entity';
import { MessengerService } from '../services/messenger.service';

@Injectable({
  providedIn: 'root'
})
export class ConversationResolverService implements Resolve<Conversation> {

  constructor(private router: Router,private messengerService : MessengerService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Conversation | Observable<Conversation> | Promise<Conversation> {
    let id = route.paramMap.get('idConversation');
    return this.messengerService.getConversationById(Number.parseInt(id))
    .pipe(
      take(1),
      mergeMap(conv => {
        if (conv) {
          return of(conv);
        } else { 
          this.router.navigate(['/review']);
          return EMPTY;
        }
      })
    );
  }

}
