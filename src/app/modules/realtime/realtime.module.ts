import { RealtimeRoutingModule } from './realtime-routing.module';
import { SharedModule } from './../shared/shared.module';
import { NotificationMenuComponent } from './components/notification-menu/notification-menu.component';
import { NgModule } from '@angular/core';
import { NotificationComponent } from './components/notification/notification.component';
import { MessengerPageComponent } from './pages/messenger-page/messenger-page.component';
import { ConversationListComponent } from './components/conversation-list/conversation-list.component';
import { ConversationComponent } from './components/conversation/conversation.component';
import { MessageComponent } from './components/message/message.component';
import { RealtimeEntryComponent } from './realtime-entry.component';

@NgModule({
  declarations: [RealtimeEntryComponent,NotificationComponent,NotificationMenuComponent, MessengerPageComponent, ConversationListComponent, ConversationComponent, MessageComponent],
  imports: [
    SharedModule,
    RealtimeRoutingModule
  ],
  exports : [
    RealtimeEntryComponent,
    NotificationComponent,
    NotificationMenuComponent
  ]
})
export class RealtimeModule { }
