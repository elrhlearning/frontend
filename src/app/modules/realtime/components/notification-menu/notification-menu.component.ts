import { NotificationService } from './../../services/notification.service';
import { NotificationData } from './../../helpers/notification-data.interface';
import { Component, OnInit,OnDestroy } from '@angular/core';


@Component({
  selector: 'notif-component',
  templateUrl: './notification-menu.component.html',
  styleUrls: ['./notification-menu.component.css']
})
export class NotificationMenuComponent implements OnInit,OnDestroy {
  badgeColor : string = 'primary';
  notifications : NotificationData [] = [];

  constructor(private readonly notificationService : NotificationService) { 

  }

  ngOnInit() {
    // get db notification
     this.notificationService.getNotifications()
    .subscribe(
      (data : NotificationData[])=> {
       this.notifications = data;
      }
    )
    this.notificationService.updateListNotification$
    .subscribe(
      (data : NotificationData[])=> {
        this.notifications = data;
      }
    )
  }
  ngOnDestroy(): void {
  
  }
  handleNotificationClick(notification : NotificationData) {
    notification.getHandler().handlchNotificationClick()
    .subscribe()
  }

  getNbNewNotification() : number {
    let n : number = 0;
    this.notifications.forEach((notif : NotificationData)=> {
      if(notif.getNotification().state)
        n++;
    })
    return n;
  }

  getColor() {
    if(this.getNbNewNotification() > 0)
      this.badgeColor='accent';
    else
      this.badgeColor = 'primary';
    return this.badgeColor;
  }
}
