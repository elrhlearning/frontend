import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Message } from '../../../shared/entity/message.entity';
import { MessengerService } from '../../services/messenger.service';

@Component({
  selector: 'message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit,OnChanges {
  private hour : string = undefined;

  @Input()
  message : Message;

  color : string;

  constructor(private readonly messengerService : MessengerService) { }

  ngOnInit() {
    this.setColor();
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.setColor();
  }
  getMessageHour(){
    if(!this.message)
      return '';
    if(!this.hour && this.message.dateCreation){
      let date = new Date(this.message.dateCreation);
      this.hour = `${date.getHours()}:${date.getMinutes()}`;
    }
    return this.hour;
  }
//*********************************************************************************************************************
  private setColor() {
    if(this.messengerService.isSender(this.message)){
      this.color = 'default';
      return false;
    }
    this.color = 'primary';
  }
//*********************************************************************************************************************  
}
