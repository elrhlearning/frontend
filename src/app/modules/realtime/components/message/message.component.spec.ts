import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageComponent } from './message.component';
import { Message } from '../../../shared/entity/message.entity';
import { MessengerService } from '../../services/messenger.service';
import { SharedModule } from '../../../shared/shared.module';
import { UserMenuComponent } from '../../../../components/user-menu/user-menu.component';
import { SidnavContentComponent } from '../../../../components/sidnav/sidnav-content.component';
import { DashboardPageComponent } from '../../../../pages/dashboard-page/dashboard-page.component';
import { MessengerNotificationComponent } from '../../../../components/messenger-notification/messenger-notification.component';
import { RealtimeModule } from '../../realtime.module';
import {RouterTestingModule} from '@angular/router/testing';
import { TestServiceFactory } from '../../../../testing/mocks/test-services.factory';

describe('MessageComponent', () => {
//********************************************************************************************************************  
  let component: MessageComponent;
  let fixture: ComponentFixture<MessageComponent>;
  let date = 'Feb 28 2013 19:00:00';
//********************************************************************************************************************  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations : [UserMenuComponent, SidnavContentComponent,  DashboardPageComponent, MessengerNotificationComponent],
      imports: [ SharedModule,RealtimeModule,RouterTestingModule],
      // provide the component-under-test and dependent service
      providers: [
        { provide: MessengerService, useFactory : () => {return TestServiceFactory.getMessengerServiceMock();} }
      ]
    })
    .compileComponents();
  }));
//********************************************************************************************************************
  beforeEach(() => {
    fixture = TestBed.createComponent(MessageComponent);
    component = fixture.componentInstance;
    let message = new Message();
    message.id = 1;
    message.content = 'test content';
    message.dateCreation = date;
    component.message = message;
  });
//********************************************************************************************************************
  it('should create', () => {
    expect(component).toBeTruthy();
  });
//********************************************************************************************************************
  it('should have default color after Angular calls ngOnInit', () => {
    component.ngOnInit();
    expect(component.color).toEqual('default');
    expect(component.message.content).toEqual('test content');
    component.message.id = 2;
    fixture.detectChanges();
    expect(component.color).toEqual('primary');
  });
//********************************************************************************************************************
  it('should test hour', () => {
    component.ngOnInit();
    fixture.detectChanges();
    expect(date).toContain(component.getMessageHour());
  });
//********************************************************************************************************************  
});
