import { NotificationService } from './../../services/notification.service';
import { NotificationData } from './../../helpers/notification-data.interface';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {
  @Input()
  notification : NotificationData;

  @Output()
  onNotificationClick : EventEmitter<NotificationData> = new EventEmitter();

  icon : string;
  constructor() { }

  ngOnInit() {
  }

  getNgClass() : Object {
    return {
      'new-notification' : this.notification.getNotification().state
    };
  }

  sendClickedNotification() {
    this.onNotificationClick.emit(this.notification);
  }
}
