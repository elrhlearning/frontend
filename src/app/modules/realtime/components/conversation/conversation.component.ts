import { Component, OnInit, OnChanges, ViewChild, ElementRef, AfterViewChecked, SimpleChanges, HostListener, EventEmitter, Output } from '@angular/core';
import { Conversation } from '../../../shared/entity/conversation.entity';
import { Message } from '../../../shared/entity/message.entity';
import { MessengerService } from '../../services/messenger.service';
import { LoadingCommanderService } from '../../../shared/services/loading-commander.service';
import { AuthService } from '../../../../services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, concatMap } from 'rxjs/operators';

@Component({
  selector: 'conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.css']
})
export class ConversationComponent implements OnInit,OnChanges, AfterViewChecked{
//*********************************************************************************************************************
  conversation : Conversation;

  newMessage : Message = new Message();

  @ViewChild('conversationBody') 
  private myScrollContainer: ElementRef;

  @Output()
  onMessageSent : EventEmitter<Conversation> = new EventEmitter();

  private isScrolledFirstTime  : number = 0;

  scrollToLastMessage : boolean = true;
//*********************************************************************************************************************
  constructor(private readonly messengerService : MessengerService, 
              private readonly authService : AuthService,
              private route: ActivatedRoute,
              private readonly loader : LoadingCommanderService) { 
    
              this.messengerService.conversationListUpdate$
              .subscribe(
                (data : any) => {
                  this.scrollToLastMessage = true;
                }
              )
  }
//*********************************************************************************************************************
  ngOnInit() { 
    // load conversation from route
    this.getConversation()
    .pipe(
      concatMap((data : Conversation) => {
        // if first loading (no messages than load)
        if(data.messages && data.messages.length <= 20){
          return this.loadMessages();
        }
        // else return current messages
        return of(data.messages);
      })
    )
    .subscribe((data : Message[])=> {
      // scroll to last message
      this.scrollToLastMessage = true;
    });
    
  }
//*********************************************************************************************************************
  ngOnChanges(changes: SimpleChanges): void {

  }
//*********************************************************************************************************************
  getMessageAlign(message : Message) : string {
    if(this.messengerService.isSender(message))
      return 'end';
    return 'start;'
  }
//*********************************************************************************************************************  
  ngAfterViewChecked(): void {
    if(this.scrollToLastMessage)
      this.scrollToBottom();
    this.scrollToLastMessage = false;
  }  
//*********************************************************************************************************************
  onScroll(event: any) {
    this.isScrolledFirstTime++;
    if (event.target.scrollTop == 0 && this.isScrolledFirstTime > 1) {
      this.loadMessages().subscribe();
    }
  }
//*********************************************************************************************************************
  private scrollToBottom(): void {
    try {
        this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight + this.myScrollContainer.nativeElement.offsetHeight;
    } catch(err) { 
    }                 
  }
//*********************************************************************************************************************
  private loadMessages() : Observable<Message[]>{
    this.loader.startLoading();
    return this.messengerService.loadMessages(this.conversation)
    .pipe(
      map((data : Message[]) => {
        data.forEach((message : Message)=>{
          this.conversation.messages.unshift(message);
        });
        this.loader.stopLoading();
        return data;
      }
    ));
  }
//*********************************************************************************************************************
  addMessage(){
    // save conv (First message)
    if(!this.conversation.messages || this.conversation.messages.length == 0){
        this.conversation.messages = [];
        this.newMessage.sender = this.authService.getMinimizedLoggedUser()
        this.newMessage.dateCreation = new Date().toISOString();
        this.conversation.messages.push(this.newMessage);
        this.messengerService.saveConversation(this.conversation)
        .subscribe(
          (data) => {
            this.conversation.id = data.idConversation;
            this.conversation.messages[0].id = data.idMessage;
            this.scrollToLastMessage = true;
            this.newMessage = new Message();
            this.onMessageSent.emit(this.conversation);
          }
        );
    }
    // add message
    else if(this.newMessage.content && this.newMessage.content.length > 0){
      this.newMessage.sender = this.authService.getMinimizedLoggedUser()
      this.newMessage.dateCreation = new Date().toISOString();
      this.newMessage.conversation=new Conversation();
      this.newMessage.conversation.id = this.conversation.id;
      this.messengerService.addMessage(this.newMessage)
      .subscribe(
        (data : number) => {
          this.scrollToLastMessage = true;
          this.newMessage.id = data;
          this.conversation.messages.push(this.newMessage);
          this.newMessage = new Message();
          this.onMessageSent.emit(this.conversation);
        }
      );
      
    }
  }
//*********************************************************************************************************************  
  getConversationTitle() : string{
    if(!this.conversation || !this.conversation.users || !(this.conversation.users.length >= 2))
      return '';
    if(this.conversation.users.length == 2)
      return  this.conversation.users[0].id == this.authService.getMinimizedLoggedUser().id 
              ? `${this.conversation.users[1].name} ${this.conversation.users[1].lastname}`
              : `${this.conversation.users[0].name} ${this.conversation.users[0].lastname}`;
    return '';
  }
//*********************************************************************************************************************
  private getConversation() : Observable<Conversation>{
    this.loader.startLoading();
    return this.route.data
    .pipe(
      map((data: { conv: Conversation }) => {
        this.conversation = data.conv;
        this.isScrolledFirstTime = 0;
        this.loader.stopLoading();
        return this.conversation;
      })
    );
  }  
//*********************************************************************************************************************
  onConversationFocused(){
    this.messengerService.handleConversationFocused(this.conversation)
    .subscribe(
      (data : any) => {
        this.scrollToBottom();
      }
    )
  }
//*********************************************************************************************************************  
}
