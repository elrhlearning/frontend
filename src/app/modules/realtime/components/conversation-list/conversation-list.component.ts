import { Component, OnInit, EventEmitter,SimpleChanges, Input, Output,OnChanges } from '@angular/core';
import { Conversation } from '../../../shared/entity/conversation.entity';
import { MessengerService } from '../../services/messenger.service';
import { AuthService } from '../../../../services/auth.service';


@Component({
  selector: 'conversation-list',
  templateUrl: './conversation-list.component.html',
  styleUrls: ['./conversation-list.component.css']
})
export class ConversationListComponent implements OnInit,OnChanges {
//*************************************************************************************************************
  constructor( 
    private readonly authService : AuthService,
    private readonly messengerService : MessengerService,
  ) {

  }
//*************************************************************************************************************  
  @Input()
  conversations : Conversation[];

  @Input()
  private selectedConversation : Conversation;

  @Output()
  onSelectConversation : EventEmitter<Conversation> = new EventEmitter();
//*************************************************************************************************************
  ngOnInit() {
  
  }
//*************************************************************************************************************
  ngOnChanges(changes: SimpleChanges): void {

  }
//*************************************************************************************************************
  selectConversation(c : Conversation){
    this.selectedConversation = c;
    this.onSelectConversation.emit(c);
  }
//*************************************************************************************************************
  getConvItemSubtitle(conv : Conversation) : string{
    if(conv && conv.messages && conv.messages.length >= 1 ){
      let senderName : string = 'You';
      let lastIndex = (conv.messages.length - 1);
      if(!this.messengerService.isSender(conv.messages[lastIndex]))
        senderName = conv.messages[lastIndex].sender.name;
      return `${senderName} : ${conv.messages[lastIndex].content}`;
    }
    return '';
  }
//*************************************************************************************************************
  getConvItemTitle(conv : Conversation) : string{
    if(!conv.users || !(conv.users.length >= 2))
      return '';
    if(conv.users.length == 2)
      return  conv.users[0].id == this.authService.getMinimizedLoggedUser().id 
              ? `${conv.users[1].name} ${conv.users[1].lastname}`
              : `${conv.users[0].name} ${conv.users[0].lastname}`;
    return '';
  }
//*************************************************************************************************************
  getConvItemDate(conv : Conversation){
    if(conv && conv.messages && conv.messages.length >= 1 ){
      let lastIndex = (conv.messages.length - 1);
      return conv.messages[lastIndex].dateCreation;
    }
    return '';
  }
//*************************************************************************************************************
  getNgStyle(conversation : Conversation) : any {
    return {
      'background-color' : this.selectedConversation && (conversation.id == this.selectedConversation.id) ? '#e2e6fb' : 'transparent'
    }
  }  
//*************************************************************************************************************  
  showNewMessagesIcon(conv : Conversation) : boolean {
    return conv && conv.countNewMessages > 0;
  }
//*************************************************************************************************************  
}
