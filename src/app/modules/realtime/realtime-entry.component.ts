import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'realtime-entry-component',
  template: `
      <router-outlet></router-outlet>
  `,
  styles : [
    `
    :host{
      height: 100%;
      margin : 0;
      padding : 0;
    }

    `
  ]
})
export class RealtimeEntryComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
