import { Conversation } from '../../shared/entity/conversation.entity';
import { User } from '../../shared/entity/user.entity';
export class UserConversationDTO {
    user : User;
    conversation : Conversation;
}