import { MessengerPageComponent } from './pages/messenger-page/messenger-page.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConversationComponent } from './components/conversation/conversation.component';
import { ConversationResolverService } from './resolvers/conversation-resolver.service';

export const realtimeModuleRoutes: Routes = [
  {
    path : '', 
    component : MessengerPageComponent,
    children : [
      {
        path : ':idConversation', 
        component : ConversationComponent,
        resolve : {
          conv : ConversationResolverService
        }
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild([])],
  exports: [RouterModule]
})
export class RealtimeRoutingModule { }
