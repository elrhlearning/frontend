import { Component, OnInit,OnDestroy } from '@angular/core';
import { MessengerService } from '../../services/messenger.service';
import { Conversation } from '../../../shared/entity/conversation.entity';
import { LoadingCommanderService } from '../../../shared/services/loading-commander.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'messenger-page',
  templateUrl: './messenger-page.component.html',
  styleUrls: ['./messenger-page.component.css']
})
export class MessengerPageComponent implements OnInit,OnDestroy {
//*********************************************************************************************************************  
  conversations : Conversation[];
  selectedConversation : Conversation = undefined;
  convListFlex='100%';
  convFlex='0%';
  private conversationOpened = false;
//*********************************************************************************************************************  
  constructor(
    private readonly messengerService : MessengerService,
    private readonly activateRoute : ActivatedRoute,
    private readonly loader : LoadingCommanderService,
    private router : Router
  ) {

  }
//*********************************************************************************************************************
  ngOnInit() {
    this.loader.startLoading();
    // get conversations
    this.messengerService.getAllConversations()
    .subscribe(
      (data : Conversation[])=>{
        this.conversations = data;
        this.loader.stopLoading();
      }
    );
    // check if conv is selected
    if(this.conversationOpened){
      this.activateRoute.firstChild.data
      .subscribe((data: { conv: Conversation }) => {
        this.selectedConversation = data.conv;
      });
    }
    // listen to messages
    this.messengerService.conversationListUpdate$
    .subscribe(
      (data : Conversation[])=> {
        this.conversations = data;
      }
    )
  }
//*********************************************************************************************************************
  openConversation(c : Conversation){
    this.selectedConversation = c;
    this.convListFlex='25%';
    this.router.navigate([`messenger`,this.selectedConversation.id]);
  }
//*********************************************************************************************************************
  ngOnDestroy(): void {

  }
//*********************************************************************************************************************
  onActivate() {
    this.convListFlex='25%';
    this.convFlex='75%';
    this.conversationOpened = true;
  }
//*********************************************************************************************************************
  onDeactivate() {
    this.convListFlex='100%';
    this.convFlex='0%';
    this.conversationOpened = false;
    this.selectedConversation = undefined;
  }
//*********************************************************************************************************************
  showConvList() {
    return this.conversationOpened == false;
  }
//*********************************************************************************************************************
  showConv(){
    return this.conversationOpened == true;
  }
//*********************************************************************************************************************  
}
