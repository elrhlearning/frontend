import { WebSocketManager } from '../../../services/abstract-websocket.class';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import * as io from 'socket.io-client';
import { Message } from '../../shared/entity/message.entity';

@Injectable({
  providedIn: 'root'
})
export class MessengerWebsocketService extends WebSocketManager {
//*****************************************************************************************************
  private observable: Observable<Message> = new Observable();
  constructor() { 
    super()
  }
//*****************************************************************************************************
  configure(): boolean {
    try {
      if(!this.isSocketReady()){
        console.log('configuring messenger');
        this.socket = io.connect(environment.messengerWebSocketHost,{
          query: {
            user : JSON.stringify(this.client)
        }});
        this.getSocket().on('connect', function () {
          console.log('Connected to messenger websocket server');
        });
        this.getSocket().on('disconnect', function () {
          console.log('disconnect from  messenger websocket server');
        });
        this.observable = new Observable((observer)=>{ 
          this.getSocket().on('newMessage',(data)=> {
            observer.next(data.message);
          });
        }); 
        return true;
      }
      return false; 
    } catch (error) {
      return false;
    }
  } 
//*****************************************************************************************************  
  onNewMessage() : Observable<Message> {
    return this.observable;
  }
//*****************************************************************************************************
}
