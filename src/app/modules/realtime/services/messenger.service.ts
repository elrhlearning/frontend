import { Injectable } from '@angular/core';
import { User } from '../../shared/entity/user.entity';
import { Observable, of, Subject, Subscription } from 'rxjs';
import { Conversation } from '../../shared/entity/conversation.entity';
import { Message } from '../../shared/entity/message.entity';
import { HttpClient , HttpParams} from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { AuthService } from '../../../services/auth.service';
import { map, mergeMap } from 'rxjs/operators';
import { UserConversationDTO } from '../dto/user-conversation-dto';
import { MessengerWebsocketService } from './messenger-websocket.service';


@Injectable({
  providedIn: 'root'
})
export class MessengerService {
//*********************************************************************************************************************  
  private  messengerURL : string = environment.apiURL + 'messenger/';
  private  allConversationsURL : string = this.messengerURL + 'convs';
  private  addConversationURL : string = this.messengerURL + 'convs/start';
  private  conversationFocusedURL : string = this.messengerURL + 'convs/focused';
  private  loadMessagesURL : string = this.messengerURL + 'messages';
  private  addMessagesURL : string = this.messengerURL + 'messages';
  private  conversationFinder : ConversationFinder;
  private  conversations : Conversation[] = [];
  public  nbNewMessages : number = undefined;
//*********************************************************************************************************************
  // emit when conversation list is updated
  public conversationListUpdate$ = new Subject<Conversation[]>();  
//*********************************************************************************************************************
  constructor(private readonly http : HttpClient,
              private messengerWebsocketService : MessengerWebsocketService,
              private authService : AuthService) 
              { 
                this.conversationFinder = new TwoUserConversationFinder();
                this.startListeningToNewMessages();
                this.initListConversations().subscribe();
                this.authService.loggedOutUser.subscribe((value)=> {
                  if(value === true){
                    this.conversations = undefined;
                  }
                })
              }
//*********************************************************************************************************************
  /**
   * Get all user conversations (sending http request only in the first call)
   * @param user 
   */
  getAllConversations() : Observable<Conversation[]> {
      return this.conversations != undefined ? of(this.conversations) : this.initListConversations();
  }
  private initListConversations() {
        // call server
        return this.http.get<Conversation[]>(this.getAllConversationsURL())
        .pipe(
          map((convs : Conversation[]) => {
            this.conversations = convs;
            this.nbNewMessages = 0;
            this.conversations.forEach((conv : Conversation)=> {
              this.nbNewMessages += conv.countNewMessages;
            });
            this.conversationListUpdate$.next(this.conversations);
            return this.conversations;
          })
        );
  }
//*********************************************************************************************************************
  loadMessages(conversation : Conversation) : Observable<Message[]> {
    let params = new HttpParams().set('skip', conversation.messages.length.toString());
    return this.http.get<Message[]>(`${this.loadMessagesURL}/${conversation.id}`,{params : params});
  }
//*********************************************************************************************************************
  getNbNewMessages() : Observable<number> {
    return of(this.nbNewMessages);
  }
//*********************************************************************************************************************
  isSender(message : Message) : boolean {
    if(!message || !message.sender || !message.sender.id)
      return false;
    return this.authService.getMinimizedLoggedUser().id == message.sender.id;
  }
//*********************************************************************************************************************
  addMessage(message : Message) : Observable<number> {
    if(message && message.sender && message.conversation){
      this.setConvInFirstPosition(message.conversation);
      return this.http.post<number>(this.getAddMessageURL(),message);
    }
    return undefined;
  }
//*********************************************************************************************************************
  saveConversation(conversation : Conversation) : Observable<any> {
    this.setConvInFirstPosition(conversation);
    return this.http.post<any>(this.addConversationURL,conversation);
  }
//*****************************************************************************************************  
  private startConversation(user : User) : Conversation {
    let conversation : Conversation = new Conversation();
    if(user != undefined && user.id != undefined){ 
      conversation.users = [];
      conversation.users.push(this.authService.getMinimizedLoggedUser());
      conversation.users.push(user);
      conversation.dateCreation = new Date().toISOString();
      conversation.messages = [];
      if(this.conversations && this.conversations.length > 0)
        conversation.id = this.conversations[(this.conversations.length - 1)].id + 1;
      else
        conversation.id = 1;
    }
    return conversation;
  }
//*****************************************************************************************************
  getConversation(user : User) : Observable<Conversation>{
    return this.getAllConversations()
    .pipe(
      mergeMap((conversations : Conversation[]) => {
        let conversation : Conversation;
        conversation = this.conversationFinder.getConversation(conversations,this.authService.getMinimizedLoggedUser(),user);
        if(!conversation){
          conversation = this.startConversation(user);
          this.conversations.unshift(conversation);
        }
        return of(conversation);
      })
    );
  }  
//***************************************************************************************************** 
  getConversationById(id : number) : Observable<Conversation> {
      return this.getAllConversations()
      .pipe(
        map((convs : Conversation[]) => {
          this.conversations = convs;
          for(let c of this.conversations){
            if(c.id == id)
              return c;
          }
        })
      )
  }
//*****************************************************************************************************
  handleConversationFocused(conversation : Conversation) : Observable<boolean> {
    if(this.convHasNewMessage(conversation)){
      let conv = new Conversation();
      conv.id = conversation.id;
      let user = new User();
      user.id = this.authService.getMinimizedLoggedUser().id;
      let dto = new UserConversationDTO();
      dto.user = user;
      dto.conversation = conversation;
      this.markMessagesAsReas(conversation);
      return this.http.post<boolean>(this.conversationFocusedURL,dto);
    }
    return of(false);
  }
//*****************************************************************************************************
  private markMessagesAsReas(conversation : Conversation) {
    if(!conversation && !conversation.messages)
    return false;
    // mark messages as read
    conversation.messages.forEach((message : Message)=> {
      if(message.sender.id != this.authService.getMinimizedLoggedUser().id && !message.read){
        message.read = true;
      }
    });
    // set count
    this.nbNewMessages = this.nbNewMessages - conversation.countNewMessages;
    conversation.countNewMessages = 0;
  }
//*****************************************************************************************************
  convHasNewMessage(conversation : Conversation){
    if(!conversation && !conversation.messages)
      return false;
    let r = false;
    conversation.messages.forEach((message : Message)=> {
      if(message.sender.id != this.authService.getMinimizedLoggedUser().id && !message.read){
        r = true;
        return r;
      }
    });
    return r;
    }  
//*****************************************************************************************************
  private setConvInFirstPosition(conversation : Conversation){
    let originConv = this.conversations.find(c => c.id == conversation.id);
    this.conversations = this.conversations.filter(item => item.id !== originConv.id);
    this.conversations.unshift(originConv);
    this.conversationListUpdate$.next(this.conversations);
  } 
//*****************************************************************************************************
  private renderNewMessage(data : Message){
    this.conversations.forEach((conv)=> {
      if(conv.messages.length == 0 && conv.id == data.conversation.id){
        conv.messages.push(data);
        conv.countNewMessages++;
      }
    })
  }
//*****************************************************************************************************
  private startListeningToNewMessages(){
    this.messengerWebsocketService.onNewMessage()
    .subscribe(
      (data : Message)=> {
        if(data) {
          this.nbNewMessages++;
          this.renderNewMessage(data);
          this.setConvInFirstPosition(data.conversation);
          this.conversationListUpdate$.next(this.conversations);
        }
      }
    )
  }
//*****************************************************************************************************    
  public getAllConversationsURL() : string {
    return `${this.allConversationsURL}/${this.authService.getMinimizedLoggedUser().id}`;
  }

  public getLoadMessagesURL(conv : Conversation) : string {
    return `${this.loadMessagesURL}/${conv.id}`;
  }

  public getAddMessageURL() : string{
    return this.addMessagesURL;
  }
}











interface ConversationFinder {
  getConversation(conversations : Conversation[],loggedUSer : User,user : User) : Conversation;
}
//*****************************************************************************************************
class TwoUserConversationFinder implements ConversationFinder{
  getConversation(conversations: Conversation[],loggedUSer : User,user : User): Conversation {
    let conversation : Conversation;
    conversations.forEach((conv : Conversation)=> {
      if(conv.users)
        if(conv.users.length == 2){
          if((conv.users[0].id == loggedUSer.id && conv.users[1].id == user.id) 
            ||
            (conv.users[1].id == loggedUSer.id && conv.users[0].id == user.id) )
            {
              conversation = conv;
              return conversation;
            }
        }
    });
    return conversation;
  }
}
