import { mergeMap,map } from 'rxjs/operators';
import { Review } from './../../shared/entity/review.entity';
import { environment } from './../../../../environments/environment';
import { HttpErrorHandler } from '../../../services/http-errors-handler.service';
import { HttpClient } from '@angular/common/http';
import { User } from './../../shared/entity/user.entity';
import { AuthService } from './../../../services/auth.service';
import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { Subscribtion } from '../../shared/entity/subscribtion.entity';

@Injectable({
  providedIn: 'root'
})
export class SubscribtionService {
//***************************************************************************************************** 
  private loggedUserSubscibtions : Subscribtion[] = null;
  public getUserSubscribtionsURL = environment.apiURL + 'subscrib/userSubscribtions';
  private startSubscribtionsURL = environment.apiURL + 'subscrib/start';
  private stopSubscribtionsURL = environment.apiURL + 'subscrib/stop';
  private startSubscriptionListener : Subject<Subscribtion> = new Subject();
  private stopSubscriptionListener : Subject<Subscribtion> = new Subject();
//*****************************************************************************************************  
  constructor(private readonly authService : AuthService,
              private readonly http: HttpClient) {
  }
//*****************************************************************************************************
  initLoggedUserSubscribtion() : Observable<boolean> {
    if(this.loggedUserSubscibtions  == null){
      let user : User = this.authService.getMinimizedLoggedUser();
      return this.http.post<Subscribtion[]>(this.getUserSubscribtionsURL,user)
              .pipe(
                map((data : Subscribtion[])=> {
                  this.loggedUserSubscibtions = data;
                  return true;
                })
              );
    }
    return of(true);
  }
//********************************** *******************************************************************
  isSubscribedToReview(review : Review) : Observable<Subscribtion> {
    return this.initLoggedUserSubscribtion()
    .pipe(
      mergeMap((data : boolean)=> {
        if(data)
        return of(this.loggedUserSubscibtions.find((e : Subscribtion) => e.review.id == review.id))
      })
    );
  }
//*****************************************************************************************************
  public startSubscribtion(review : Review,user? : User) : Observable<Subscribtion>{
    if(!user)
      user = this.authService.getMinimizedLoggedUser();
    return this.http.post<Subscribtion>(this.getStartSubscriptionURL(review,user),{})
    .pipe(
      map((subsc  : Subscribtion) => {
        if(!subsc)
          return null;
        if(this.loggedUserSubscibtions == null)
          this.loggedUserSubscibtions = [];
        this.loggedUserSubscibtions.push(subsc);
        this.startSubscriptionListener.next(subsc);
        return subsc;
      })
    )
  }
//*****************************************************************************************************
  public stopSubscription( subc :Subscribtion) : Observable<boolean>{
    return this.http.post<boolean>(`${this.stopSubscribtionsURL}`,subc)
    .pipe(
      map((data  : boolean) => {
        if(!data)
          return false;
        // delete subscribtion from liste  
        this.loggedUserSubscibtions = this.loggedUserSubscibtions.filter((s : Subscribtion)=> {
          return s.id != subc.id;
        });
        // emit from listener
        this.stopSubscriptionListener.next(subc);
        // return ok
        return true;
      })
    )
  }
//***************************************************************************************************** 
  public onStartSubscription() : Observable<Subscribtion>{
    return this.startSubscriptionListener;
  } 
//*****************************************************************************************************
  public onStopSubscription() : Observable<Subscribtion>{
    return this.stopSubscriptionListener;
  }
//*****************************************************************************************************
  public getStartSubscriptionURL(review : Review, user : User){
    return `${this.startSubscribtionsURL}/${user.id}/${review.id}`;
  }  

  public getStopSubscriptionURL(){
    return this.stopSubscribtionsURL;
  }
}
