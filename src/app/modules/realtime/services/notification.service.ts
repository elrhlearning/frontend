import { AuthService } from './../../../services/auth.service';
import { environment } from './../../../../environments/environment';
import { map } from 'rxjs/operators';
import { NotificationFactoryService } from './../helpers/notification-factory.service';
import { WebsocketService } from './../../../services/websocket.service';
import { Observable, of, Subject, Subscription } from 'rxjs';
import { Injectable } from '@angular/core';
import {NotificationData} from './../helpers/notification-data.interface';
import { Notification } from './../../shared/entity/notifications/notification.abstract.entity';
import { HttpClient } from '@angular/common/http';
import { SubscribtionService } from './subscribtion.service';
import { Subscribtion } from '../../shared/entity/subscribtion.entity';
import { ReviewNotification } from '../../shared/entity/notifications/review-notification.entity';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
//*****************************************************************************************************  
  observable:Observable<NotificationData>;  
  private getNotificationsURL : string = environment.apiURL + 'notification';
  private markAsReadURL : string = environment.apiURL + 'notification/read';
  private deleteNotificationURL : string = environment.apiURL + 'notification/delete';
  notifications : NotificationData [] = undefined;
//*****************************************************************************************************
  updateListNotification$ = new Subject<NotificationData[]>();  
//*****************************************************************************************************  
  constructor(private readonly websocketService : WebsocketService,
              private readonly http : HttpClient,
              private authService : AuthService,
              private readonly subscriptionService : SubscribtionService,
              private readonly notifFactory : NotificationFactoryService) { 
    // prepare factory            
    this.notifFactory.setNotificationService(this); 
    // listen to new notifications
    this.websocketService.onNewNotification()
    .subscribe(
      (data : NotificationData)=> {
        this.notifications.unshift(data);
        this.updateListNotification$.next(this.notifications);
      }
    )
    // listen to subscriptions
    this.subscriptionService.onStartSubscription()
    .subscribe(
      (data : Subscribtion)=>{
      }
    );
    this.subscriptionService.onStopSubscription()
    .subscribe(
      (data : Subscribtion)=>{
        this.deleteSubscriptionNotifications(data);
        this.updateListNotification$.next(this.notifications);
      }
    );
  }
//*****************************************************************************************************
  public getNotifications() : Observable<NotificationData[]>{
    if(!this.notifications)
    return this.http.post<Notification[]>(this.getNotificationsURL,this.authService.getMinimizedLoggedUser())
    .pipe(
      map( (notifs : Notification[]) => {
        this.notifications = [];
        notifs.forEach((n : Notification) =>{
          this.notifications.push(this.notifFactory.getNotificationData(n));
        });
        return this.notifications;
      })
    );
    return of(this.notifications);
  }
//*****************************************************************************************************  
  markAsRead(notification : NotificationData) : Observable<boolean> {
    if(notification.getNotification().state){
      this.sendNotificationListUpdate();
      return this.http.post<boolean>(this.markAsReadURL,notification.getNotification());
    }
    return of(false);
  }
//*****************************************************************************************************
  deleteNotification(notification : Notification, onlyFront?: boolean): Observable<boolean>{
    let initialLength = this.notifications.length;
    this.notifications = this.notifications.filter((notif : NotificationData) => {
      return notif.getNotification().id != notification.id;
    });
    if(initialLength > this.notifications.length){
      this.sendNotificationListUpdate();
      if(onlyFront)
        return of(true);
      else
      return this.http.post<boolean>(this.deleteNotificationURL,notification);
    }
    return of(false);
  }
//*****************************************************************************************************  
  sendNotificationListUpdate(){
    this.updateListNotification$.next(this.notifications);
  }
//*****************************************************************************************************
  private deleteSubscriptionNotifications(subs : Subscribtion) {
    this.notifications = this.notifications.filter((notif : NotificationData)=> {
        if(notif && notif.getNotification() && (<ReviewNotification>notif.getNotification()).subscribtion)
          return (<ReviewNotification>notif.getNotification()).subscribtion.id != subs.id;
        return false;
    });
  }  
}
