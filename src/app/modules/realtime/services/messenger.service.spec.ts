import { TestBed } from '@angular/core/testing';
import { AuthService } from '../../../services/auth.service';
import { TestServiceFactory } from '../../../testing/mocks/test-services.factory';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MessengerService } from './messenger.service';
import { Conversation } from '../../shared/entity/conversation.entity';
import { Message } from '../../shared/entity/message.entity';


describe('MessengerService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let messengerService : MessengerService;
  let req;
  let convs = TestServiceFactory.getMessengerServiceMock().getAllConversations();
//*********************************************************************************************************************
  beforeAll(() => {
    TestBed.configureTestingModule({
      // Import the HttpClient mocking services
      imports: [ HttpClientTestingModule ],
      // Provide the service-under-test
      providers: [ 
        MessengerService,
        { provide: AuthService, useFactory : () => { return TestServiceFactory.getAuthServiceMock()} }, 
      ],
      schemas: [ NO_ERRORS_SCHEMA ],
    });

    // Inject the http, test controller, and service-under-test
    // as they will be referenced by each test.
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    messengerService = TestBed.get(MessengerService);
    // after instance creation a request is called, we check data
    req = httpTestingController.expectOne(messengerService.getAllConversationsURL());
    expect(req.request.method).toEqual('GET');
    req.flush(convs);
    // prepare data that it will be used before each test (subscribtions)
    messengerService.getAllConversations().subscribe(
      result => expect(result).toEqual(convs,'should return list of conversations'),
      fail
    ); 
  });
//*********************************************************************************************************************
  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });
//********************************************************************************************************************* .
  it('should return list of conversations and check nb new messages', () => {
    messengerService.getAllConversations().subscribe(
      (result : Conversation[]) =>  {
        expect(result).toEqual(convs,'should return list of conversations');
        expect(result[0].countNewMessages).toEqual(0,'should return list of conversations');
        expect(result[1].countNewMessages).toEqual(1,'should return list of conversations');
        expect(result[1].messages.length).toEqual(2,'should return list of conversations');
        let r = messengerService.convHasNewMessage(result[1]);
        expect(r).toBeTruthy();
      fail
    }
    );
  }); 
//*********************************************************************************************************************
  it('should load messages of conversation ', () => {
    let msgs = TestServiceFactory.getMessengerServiceMock().loadMessages(convs[0]);
    messengerService.loadMessages(convs[0]).subscribe(
      (result : Message[]) =>  {
        expect(result).toEqual(msgs,'should return list of conversations');
        expect(result.length).toEqual(25,'should return list of conversations');
      fail
    }
    );
    // mock request result
    let url = messengerService.getLoadMessagesURL(convs[0]) + '?skip=' + convs[0].messages.length.toString();
    req = httpTestingController.expectOne(url);
    expect(req.request.method).toEqual('GET');
    req.flush(msgs);
  }); 
//*********************************************************************************************************************
  it('should add message to conversation ', () => {
    let user = TestServiceFactory.getUserServiceMock().getUser(3);
    let conv = convs[1];
    let msg = TestServiceFactory.getMessengerServiceMock().getMessageWithSenderAndConv(user,conv);
    messengerService.addMessage(msg).subscribe(
      (data : number)=> {
        expect(data).toEqual(msg.id,'addMessage - received ID is the mocked in the request');
        messengerService.getAllConversations().subscribe(
          result => {
            convs = result;
            expect(conv).toEqual(result[0],'First conv must be message conv');
            fail;
          }
        ); 
      }
    );
    // mock request result
    let url = messengerService.getAddMessageURL();
    req = httpTestingController.expectOne(url);
    expect(req.request.method).toEqual('POST');
    req.flush(msg.id);
  });
//*********************************************************************************************************************  
  it('Should get conversation of connected user with another' ,() => {
    let user = TestServiceFactory.getUserServiceMock().getUser(4);
    expect(user).toBeDefined();
    let initialConvsLength = convs.length;
    messengerService.getConversation(user)
    .subscribe(
      (data : Conversation) => {
        expect(data).toBeDefined('Should create conversation');
        expect(data.messages.length).toEqual(0,'Should create conv without messages');
        expect(data.users.length).toEqual(2,'Should create conv of 2 users');
        // check if we had aditional conv
        messengerService.getAllConversations().subscribe(
          (conversations : Conversation[])=> {
            convs = conversations;
            expect(conversations.length).toEqual((initialConvsLength+1),'Should check if conv was added');
          }
        )
      }
    )
  });
//*********************************************************************************************************************
});
