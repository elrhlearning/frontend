import { TestBed } from '@angular/core/testing';

import { SubscribtionService } from './subscribtion.service';
import { AuthService } from '../../../services/auth.service';
import { TestServiceFactory } from '../../../testing/mocks/test-services.factory';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { asyncData } from '../../../testing/async-observable-helpers';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Subscribtion } from '../../shared/entity/subscribtion.entity';


describe('SubscribtionService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let subscriptionService : SubscribtionService;
  let req;
  let newSubscription;
  let subs;
//*********************************************************************************************************************
  beforeEach(() => {
    TestBed.configureTestingModule({
      // Import the HttpClient mocking services
      imports: [ HttpClientTestingModule ],
      // Provide the service-under-test
      providers: [ 
        SubscribtionService,
        { provide: AuthService, useFactory : () => { return TestServiceFactory.getAuthServiceMock()} }, 
      ],
      schemas: [ NO_ERRORS_SCHEMA ],
    });

    // Inject the http, test controller, and service-under-test
    // as they will be referenced by each test.
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    subscriptionService = TestBed.get(SubscribtionService);
    // prepare data that it will be used before each test (subscribtions)
    subs = TestServiceFactory.getSubscriptionServiceMock().getConnectedUserSubscriptions();
    subscriptionService.initLoggedUserSubscribtion().subscribe(
      result => expect(result).toEqual(true, 'should return true after init logged user subscription'),
      fail
    ); 
    req = httpTestingController.expectOne(subscriptionService.getUserSubscribtionsURL);
    expect(req.request.method).toEqual('POST');
    req.flush(subs);
  });
//*********************************************************************************************************************
  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });
//*********************************************************************************************************************
  it('should return true because connected user is sunscribed to review', () => {
    subscriptionService.isSubscribedToReview(subs[0].review)
    .subscribe(
      (data : Subscribtion ) => {
        expect(data).toBeDefined('should return true because connected user is sunscribed to review')
        expect(data.id).toEqual((subs[0]).id,'should return true because connected user is sunscribed to review')
      }
    )  
  });
//*********************************************************************************************************************
  it('should return false because connected user is sunscribed to review', () => {
    subscriptionService.isSubscribedToReview(TestServiceFactory.getSubscriptionServiceMock().getSubscriptionOfOtherUser().review)
    .subscribe(
      (data : Subscribtion ) => {
        expect(data).toBeUndefined('should return true because connected user is sunscribed to review')
      }
    )  
  });  
//*********************************************************************************************************************
  it('should startSubscription ', () => {
    let review = TestServiceFactory.getSubscriptionServiceMock().getSubscriptionOfOtherUser().review;
    let loggedUser = TestServiceFactory.getAuthServiceMock().getMinimizedLoggedUser();
    // new subscription expected result
    newSubscription = new Subscribtion();
    newSubscription.subscribed = loggedUser;
    newSubscription.review = review;
    newSubscription.dateCreation = new Date().toISOString();
    newSubscription.id = 963258714;
    // call service
    subscriptionService.startSubscribtion(review,loggedUser)
    .subscribe(
      (data : Subscribtion ) => {
        expect(data).toBeDefined('should return true because connected user is sunscribed to review')
        expect(data.id).toEqual(newSubscription.id,'should return true because connected user is sunscribed to review')
        subs.push(data);
      }
    )  
    req = httpTestingController.expectOne(subscriptionService.getStartSubscriptionURL(review,loggedUser));
    expect(req.request.method).toEqual('POST');
    req.flush(newSubscription);
  });
//*********************************************************************************************************************
  it('should  stopSubscription ', () => {
    // call service
    subscriptionService.stopSubscription(newSubscription)
    .subscribe(
      (data : boolean ) => {
        expect(data).toBeDefined('should return true because connected user is sunscribed to review')
        expect(data).toEqual(true,'should return true because connected user is sunscribed to review')
      }
    )  
    req = httpTestingController.expectOne(subscriptionService.getStopSubscriptionURL());
    expect(req.request.method).toEqual('POST');
    req.flush(newSubscription);
  });
//*********************************************************************************************************************
});
