import { ReviewNotification } from './../../shared/entity/notifications/review-notification.entity';
import { Router} from '@angular/router';
import { Injectable } from '@angular/core';
import { NotificationHandler } from './notificattion-handler.interface';
import { Notification } from '../../shared/entity/notifications/notification.abstract.entity';
import { of } from 'rxjs';
import { AbstractNotificationHandler } from './abstract-notification.handler';


@Injectable({
    providedIn: 'root'
  })
export class ReviewNotificationHandler extends AbstractNotificationHandler implements NotificationHandler {
    constructor(private router : Router){
      super();
    }
    handlchNotificationClick() {
        this.markAsRead().subscribe();
        let distURL = `/review/${this.cast(this.notification.getNotification()).subscribtion.review.id}`;
        if(distURL == this.router.url)
              this.router.navigate([`${distURL}?source=notif`]);
        else
            this.router.navigate([distURL]);
        // ok
        return of(true);
    }

    private cast(notification : Notification) : ReviewNotification {
      return (<ReviewNotification>notification);
    }
}