import { NotificationHandler } from './notificattion-handler.interface';
import { NotificationData } from './notification-data.interface';
import { NotificationService } from '../services/notification.service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
export abstract class AbstractNotificationHandler implements NotificationHandler {
    protected notification : NotificationData;
    protected notifService :  NotificationService;

    markAsRead() : Observable<boolean>{
        if(this.notification.getNotification().state)
        return this.notifService.markAsRead(this.notification)
        .pipe(
            map( (data : boolean)=> {
                this.notification.getNotification().state = false;
                return data;
            })
        )
        else
            return of(false);
    }

    setNotification(notificaton: NotificationData) {
        this.notification = notificaton;
    }
    setNotificationService (notificationService : NotificationService){
        this.notifService = notificationService;
    }
    /**
     * Method to implement on each type of notification
     */
    abstract handlchNotificationClick();
}