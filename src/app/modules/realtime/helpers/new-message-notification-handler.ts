import { Router} from '@angular/router';
import { Injectable } from '@angular/core';
import { NotificationHandler } from './notificattion-handler.interface';
import { Notification } from '../../shared/entity/notifications/notification.abstract.entity';
import { NewMessageNotification } from '../../shared/entity/notifications/new-message.notification.entity';
import { NotificationService } from '../services/notification.service';
import { AbstractNotificationHandler } from './abstract-notification.handler';
import { of } from 'rxjs';

@Injectable({
    providedIn: 'root'
  })
export class NewMessageNotificationHandler extends AbstractNotificationHandler implements NotificationHandler{

    constructor(private router : Router){
      super();
    }
    handlchNotificationClick() {
        // delete notif from server
        this.notifService.deleteNotification(this.notification.getNotification()).subscribe();
        // go to messenger
        let distURL = `/messenger`;
        if(distURL == this.router.url)
              this.router.navigate([distURL,this.cast(this.notification.getNotification()).newMessage.conversation.id],{
                  queryParams : { 
                    source : 'notif'
                  }
                  });
        else
              this.router.navigate([distURL,this.cast(this.notification.getNotification()).newMessage.conversation.id]);
        // ok
        return of(true);
    }

    private cast(notification : Notification) : NewMessageNotification {
      return (<NewMessageNotification>notification);
    }

    setNotificationService (notificationService : NotificationService){
      this.notifService = notificationService;
    }
}