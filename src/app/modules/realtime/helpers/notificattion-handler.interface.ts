import { Notification } from '../../shared/entity/notifications/notification.abstract.entity';
import { NotificationData } from './notification-data.interface';
import { Observable } from 'rxjs';

export interface NotificationHandler{
    setNotification(notificaton : NotificationData);
    handlchNotificationClick() : Observable<boolean>;
}