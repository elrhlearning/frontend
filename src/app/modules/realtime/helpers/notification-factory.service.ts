import { ReviewNotificationHandler } from './review-notification-handler';
import { NotificationHandler } from './notificattion-handler.interface';
import { NotificationData } from './notification-data.interface';
import { SubscribeReviewNotification } from './../../shared/entity/notifications/sunscribe-notification.entity';
import { Notification } from './../../shared/entity/notifications/notification.abstract.entity';
import { Injectable } from '@angular/core';
import { UpdateReviewNotification } from '../../shared/entity/notifications/update-review-notification.entity';
import { CommentedReviewNotification } from '../../shared/entity/notifications/commented-review-notification.entity';
import { NewMessageNotification } from '../../shared/entity/notifications/new-message.notification.entity';
import { NewMessageNotificationHandler } from './new-message-notification-handler';
import { NotificationService } from '../services/notification.service';
import { UnreadMessagesNotification } from '../../shared/entity/notifications/unread-messages.notification.entity';
import { UnreadMessagesNotificationHandler } from './unread-messages-notification-handler';
//**************************************************************************************************************************************
/**
 * class that create NotificationData depending on notification object received from 
 * API
 * Class should inject a handler of each type of notification,For the time being we have only reviewNotificationHandler
 * When new type of notification is created :
 *  1-  create Entity needed
 *  2-  create handler needed (implement NotificationHandler)
 *  3-  create NotificationData for this type of notification (implement NotificationData)
 *  4-  inject handler in this class
 *  5-  add if clause in getNotificationData() method to indentify which type of notification is comming
 *      from backend 
 *  6-  use 2 and 3 to return instance of NotificationData from getNotificationData() method
 */
@Injectable({
providedIn: 'root'
})
export class NotificationFactoryService {
    private  notifService : NotificationService;
    constructor(
      private readonly reviewNotificationHandler : ReviewNotificationHandler,
      private readonly newMessageNotificationHandler : NewMessageNotificationHandler,
      private readonly unreadMessagesNotificationHandler : UnreadMessagesNotificationHandler,
    ){
        
    }    
    getNotificationData(notification : any) : NotificationData{
        if(!notification)
            return undefined;
        // update notification
        if(notification.dateModification && notification.comment){
            let s  = new CommentedReviewNotificationData(notification,this.reviewNotificationHandler);
            let r : NotificationData = (<NotificationData>s);
            return r;
        }
        // update notification
        if(notification.dateModification){
            let s  = new UpdateNotificationData(notification,this.reviewNotificationHandler);
            let r : NotificationData = (<NotificationData>s);
            return r;
        }
        // subscribe notification
        if(notification.subscribtion){
            let s  = new SubscribNotificationData(notification,this.reviewNotificationHandler);
            let r : NotificationData = (<NotificationData>s);
            return r;
        }

        return undefined;
    }
//**************************************************************************************************************************************    
    setNotificationService (notificationService : NotificationService){
        this.notifService = notificationService;
        this.newMessageNotificationHandler.setNotificationService(notificationService);
        this.unreadMessagesNotificationHandler.setNotificationService(notificationService);
        this.reviewNotificationHandler.setNotificationService(notificationService);
    }
}
//**************************************************************************************************************************************   
/**
 * class that implement NotificationData and it is used for notification of type "Subscribe to review" 
 * constructor 
 */
class SubscribNotificationData implements NotificationData{
//*********************************************************************************************************    
    private notification : SubscribeReviewNotification;
    private notificationHandler : NotificationHandler;
//*********************************************************************************************************    
    constructor(notification : SubscribeReviewNotification,notificationHandler : NotificationHandler){
        this.notification = <SubscribeReviewNotification> notification;
        this.notification.subscribtion = this.notification.subscribtion;
        this.notificationHandler = notificationHandler;
    }   
//*********************************************************************************************************    
    getIcon(): string {
        return 'touch_app';
    }    
//*********************************************************************************************************    
    getTitle(): string {
        return "New Subscription";
    }
//*********************************************************************************************************    
    getDesc(): string {
        return `User ${this.notification.subscribtion.subscribed.name} start 
                following your Review : ${this.notification.subscribtion.review.title}`;
    }
//*********************************************************************************************************    
    getDate() : string {
        return `Subscription Date : ${this.notification.subscribtion.dateCreation}`;
    }
//*********************************************************************************************************
    getHandler() : NotificationHandler{
        this.notificationHandler.setNotification(this);
        return this.notificationHandler;
    }
//*********************************************************************************************************    
    getNotification(): Notification {
      return this.notification;
    }
//*********************************************************************************************************  
}



//**************************************************************************************************************************************   
/**
 * class that implement NotificationData and it is used for notification of type "Subscribe to review" 
 * constructor 
 */
class UpdateNotificationData implements NotificationData{
    //*********************************************************************************************************    
        protected notification : UpdateReviewNotification;
        protected notificationHandler : NotificationHandler;
    //*********************************************************************************************************    
        constructor(notification : UpdateReviewNotification,notificationHandler : NotificationHandler){
            this.notification = <UpdateReviewNotification> notification;
            this.notification.subscribtion = this.notification.subscribtion;
            this.notificationHandler = notificationHandler;
        }   
    //*********************************************************************************************************    
        getIcon(): string {
            return 'edit';
        }    
    //*********************************************************************************************************    
        getTitle(): string {
            return "Review Modification";
        }
    //*********************************************************************************************************    
        getDesc(): string {
            return `Review ${this.notification.subscribtion.review.title} that you follow is updated, click to check`;
        }
    //*********************************************************************************************************    
        getDate() : string {
            return `Update Date : ${this.notification.dateModification}`;
        }
    //*********************************************************************************************************
        getHandler() : NotificationHandler{
            this.notificationHandler.setNotification(this);
            return this.notificationHandler;
        }
    //*********************************************************************************************************    
        getNotification(): Notification {
          return this.notification;
        }
    //*********************************************************************************************************  
    }

//**************************************************************************************************************************************   
/**
 * class that implement NotificationData and it is used for notification of type "Subscribe to review" 
 * constructor 
 */
class CommentedReviewNotificationData extends UpdateNotificationData implements NotificationData {
    //*********************************************************************************************************    
        constructor(notification : CommentedReviewNotification,notificationHandler : NotificationHandler){
            super(notification,notificationHandler);
        }   
    //*********************************************************************************************************    
        getIcon(): string {
            return 'comment';
        }    
    //*********************************************************************************************************    
        getTitle(): string {
            return "Review Commented";
        }
    //*********************************************************************************************************    
        getDesc(): string {
            return `New Comment is added to Review ${(<CommentedReviewNotification>this.notification).subscribtion.review.title} that you follow comment 
            : ${(<CommentedReviewNotification>this.notification).comment.content}, click to check`;
        }
    //*********************************************************************************************************  
}     