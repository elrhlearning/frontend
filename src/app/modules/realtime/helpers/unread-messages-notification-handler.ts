import { ReviewNotification } from '../../shared/entity/notifications/review-notification.entity';
import { Router , NavigationEnd} from '@angular/router';
import { Injectable } from '@angular/core';
import { NotificationHandler } from './notificattion-handler.interface';
import { Notification } from '../../shared/entity/notifications/notification.abstract.entity';
import { NotificationData } from './notification-data.interface';
import { NewMessageNotification } from '../../shared/entity/notifications/new-message.notification.entity';
import { NotificationService } from '../services/notification.service';
import { UnreadMessagesNotification } from '../../shared/entity/notifications/unread-messages.notification.entity';
import { of } from 'rxjs';

@Injectable({
    providedIn: 'root'
  })
export class UnreadMessagesNotificationHandler implements NotificationHandler {
    private notification : NotificationData;
    private  notifService : NotificationService;
    constructor(private router : Router){
      
    }
    handlchNotificationClick() {
        // delete notif from front only
        this.notifService.deleteNotification(this.notification.getNotification(),true).subscribe();
        // go to messenger
        let distURL = `/messenger`;
        if(distURL == this.router.url)
              this.router.navigate([distURL],{
                  queryParams : { 
                    source : 'notif'
                  }
                  });
        else
              this.router.navigate([distURL]);
        // ok
        return of(true);
    }

    private cast(notification : Notification) : UnreadMessagesNotification {
      return (<UnreadMessagesNotification>notification);
    }
    setNotification(notificaton: NotificationData) {
      this.notification = notificaton;
    }

    setNotificationService (notificationService : NotificationService){
      this.notifService = notificationService;
    }
}