import { NotificationHandler } from './notificattion-handler.interface';
import { Notification } from '../../shared/entity/notifications/notification.abstract.entity';
/**
 * Interface that represent data used by a notification
 */
export interface NotificationData {
    /**
     * get the icon 
     */
    getIcon() : string;
    /**
     * get Title
     */
    getTitle() : string;
    /**
     * get Description
     */
    getDesc() : string;
    /**
     * get Evenement date
     */
    getDate() : string;

    /**
     * get instance of a handler (to handle click,drag...)
     */
    getHandler() : NotificationHandler;

    getNotification() : Notification;
}