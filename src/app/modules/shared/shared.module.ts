import { ConfirmationDialogComponent } from './components/delete-dialog-component/confirmation-dialog.component';
import { EqualValidatorDirective } from './directives/equal-validator.directive';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {  FormsModule,ReactiveFormsModule } from '@angular/forms';
import {  MatToolbarModule,MatMenuModule,MatButtonModule,MatButtonToggleModule,MatRadioModule,
  MatIconModule,MatDialogModule,MatTooltipModule,MatCardModule,MatSidenavModule,MatSelectModule,
  MatFormFieldModule,MatInputModule,MatListModule,MatTabsModule,MatBadgeModule,MatOptionModule, 
  MatProgressBarModule,MatSnackBarModule,MatProgressSpinnerModule,MatChipsModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PageTitleDirective } from './directives/page-title.directive';
import { MarginTopAndBottomDirective } from './directives/margin-top-and-bottom.directive';
import { LoadingComponent } from './components/loading/loading.component';
import { HttpClientModule }    from '@angular/common/http';
import { UserInfoComponent } from './components/user-info/user-info.component';
import { UserInfoModalComponent } from './components/user-info-modal/user-info-modal.component';
import { LayoutModule } from '@angular/cdk/layout';
import { LinkValidatorDirective } from './directives/link-validator.directive';
import { EmailValidatorDirective } from './directives/email-validator.directive';



@NgModule({
  declarations: [ PageTitleDirective, MarginTopAndBottomDirective,LinkValidatorDirective,
                  UserInfoComponent,UserInfoModalComponent,EmailValidatorDirective,
                  LoadingComponent,EqualValidatorDirective,ConfirmationDialogComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    //material
    MatToolbarModule,MatMenuModule,MatButtonModule,MatButtonToggleModule,MatRadioModule,
    MatIconModule,MatDialogModule,MatTooltipModule,MatCardModule,MatSidenavModule,MatOptionModule,
    MatFormFieldModule,MatInputModule,MatListModule,MatTabsModule,FlexLayoutModule,MatBadgeModule,
    MatProgressBarModule,MatSnackBarModule,MatProgressSpinnerModule,MatChipsModule,
    LayoutModule,
    // http
    HttpClientModule
  ],
  exports : [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    // material exports
    MatToolbarModule,MatMenuModule,MatButtonModule,MatButtonToggleModule,MatRadioModule,
    MatIconModule,MatDialogModule,MatTooltipModule,MatCardModule,MatSidenavModule,MatOptionModule,
    MatFormFieldModule,MatInputModule,MatListModule,MatTabsModule,FlexLayoutModule,MatBadgeModule,
    MatProgressBarModule,MatSelectModule,MatProgressBarModule,MatSnackBarModule,MatProgressSpinnerModule,
    MatChipsModule,
    LayoutModule,
    // http
    HttpClientModule,
    // app exports
    PageTitleDirective,
    MarginTopAndBottomDirective,
    LinkValidatorDirective,
    EqualValidatorDirective,
    EmailValidatorDirective,
    LoadingComponent,
    ConfirmationDialogComponent,
    UserInfoComponent,UserInfoModalComponent,
  ],
  entryComponents : [ConfirmationDialogComponent,UserInfoModalComponent]
})
export class SharedModule { }
