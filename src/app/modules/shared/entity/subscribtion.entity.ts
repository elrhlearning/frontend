import { User } from './user.entity';
import { Review } from './review.entity';
export class Subscribtion {
    id : number;
    
    dateCreation : string;
    
    review : Review;
    
    subscribed : User;
}