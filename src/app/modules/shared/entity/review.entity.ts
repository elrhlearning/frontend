import { Comment } from './comment.entity';
import { User } from './user.entity';
import { ItemReview } from './item-review.entity';
import { Image } from './image.entity';
import {Type } from "class-transformer";

export class Review {

    id : number;

    title : string;

    description : string;

    link : string;
   
    dateCreation : string;

    @Type(() => ItemReview)
    items : ItemReview [];

    @Type(() => Image)
    image : Image;

    @Type(() => User)
    user : User;
    
    @Type(() => Comment)
    comments : Comment[];
}