import { User } from './user.entity';
import { Review } from './review.entity';


export class Comment {

    id : number;
    
    content : string;
    
    dateCreation : string;

    review : Review;

    owner : User;
}