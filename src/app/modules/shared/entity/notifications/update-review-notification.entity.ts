import { ReviewNotification } from './review-notification.entity';

export  class UpdateReviewNotification extends ReviewNotification{

    dateModification : string;
}