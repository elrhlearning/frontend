
import { Notification } from './notification.abstract.entity';
import { Message } from '../message.entity';

export class NewMessageNotification extends Notification{
  newMessage : Message;
}