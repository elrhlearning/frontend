import { Review } from './../review.entity';
import { Notification } from './notification.abstract.entity';
import { Subscribtion } from '../subscribtion.entity';

export abstract class ReviewNotification extends Notification{
  subscribtion : Subscribtion;
}