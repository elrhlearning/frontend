
import { Notification } from './notification.abstract.entity';


export class UnreadMessagesNotification extends Notification{

  nbMessages : number;
}