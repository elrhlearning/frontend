import { User } from './../user.entity';

export abstract class Notification {
  id : number;

  dateCreation : string;

  state : boolean = false;

  notifiedUser : User;  
}