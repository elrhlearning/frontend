import { User } from './user.entity';
import { Message } from './message.entity';


export class Conversation  {

  id : number;


  dateCreation : string;


  messages : Message[];


  users : User[];

  getConversationName() : string {
    if(this.users && this.users.length <= 2)
      return ` ${this.users[0].lastname} ${this.users[0].name}`
  }

  countMessages? : number;
  countNewMessages? : number;
}