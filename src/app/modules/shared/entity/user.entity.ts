import { Image } from './image.entity';
import { Review } from './review.entity';
export class User {
    id : number;
    name: string;
    lastname: string;
    email: string;
    phoneNumber: string;
    reviews : Review[]; 
    image : Image; 
    token : any;
    password : string;
}