export class Image {

  id : number;

  name : string;  

  path : string;

  desc : string;   

  file ? : File;
}