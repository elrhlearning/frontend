import { Conversation } from './conversation.entity';
import { User } from './user.entity';

export class Message  {

  id : number;


  dateCreation : string;


  content : string;


  read : boolean = false;


  sender : User;


  conversation : Conversation;
}