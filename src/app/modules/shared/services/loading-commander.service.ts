import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoadingCommanderService {
  public loading : boolean = false;
  public color : string  = 'primary';
  public mode : string  = 'query';
  constructor() { }

  startLoading(){
    if(!this.loading)
      this.loading = true;
  }

  stopLoading() {
    if(this.loading)
      setTimeout(() => {
        this.loading = false;
      }, 400);
  }

  changeColor(color : string){
    this.color = color;
  }
  changeMode(mode : string){
    this.mode = mode;
  }
}
