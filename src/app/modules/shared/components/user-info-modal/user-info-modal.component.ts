import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IDialogContentBuilder } from '../../../shared/components/delete-dialog-component/dialog-content-builder.interface';
import { User } from '../../entity/user.entity';
import { MessengerService } from '../../../realtime/services/messenger.service';
import { Router } from '@angular/router';
import { AuthService } from '../../../../services/auth.service';
import { Conversation } from '../../entity/conversation.entity';

@Component({
  selector: 'user-info-modal',
  templateUrl: './user-info-modal.component.html',
  styleUrls: ['./user-info-modal.component.css']
})
export class UserInfoModalComponent implements OnInit {
  user : User;
  constructor(  public dialogRef: MatDialogRef<UserInfoModalComponent>,
                private messengerService : MessengerService,
                private authService  : AuthService,
                private router : Router,
                @Inject(MAT_DIALOG_DATA) public data: IDialogContentBuilder) {

  }

  ngOnInit() {
    this.user = <User>this.data.getObject();
  }

  goToMessenger(){    
    this.messengerService.getConversation(this.user)
    .subscribe(
      (data : Conversation) => {
        this.dialogRef.close();
        this.router.navigate([`/messenger`,data.id]);
      }
    )
  }

  isLoggedUser() : boolean {
    return this.authService.getMinimizedLoggedUser().id == this.user.id;
  }
}
