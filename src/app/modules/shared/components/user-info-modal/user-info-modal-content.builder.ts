import { IDialogContentBuilder } from '../../../shared/components/delete-dialog-component/dialog-content-builder.interface';
import { User } from '../../../shared/entity/user.entity';
export class UserInfoModalContentBuilder implements IDialogContentBuilder {
    private user : User;

    constructor(user : User){
        this.user = user;
    }
    btnCancelTitle: string = 'OK';    
    btnConfirmTitle: string = undefined;

    getMessage(): string {
        return undefined;
    }
    getObject() : User {
       return this.user;
    }

    getDialogTitle(): string {
        if(!this.user || !this.user.id)
            return `User data`;
        return `User ${this.user.name} Informations`;
    }
}