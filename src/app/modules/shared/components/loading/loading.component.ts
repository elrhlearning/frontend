import { LoadingCommanderService } from './../../services/loading-commander.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'loading-bar',
  styleUrls : ['./loading-component.css'],
  templateUrl: './loading.component.html'
})
export class LoadingComponent implements OnInit {
  constructor(public loadingCommander : LoadingCommanderService) { }

  ngOnInit() {
  }
}
