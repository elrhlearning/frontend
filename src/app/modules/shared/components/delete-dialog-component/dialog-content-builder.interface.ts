export interface IDialogContentBuilder {
    btnCancelTitle : string;
    btnConfirmTitle : string;
    getMessage() : string;
    getObject() : any;
    getDialogTitle() : string;
}