import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { UserInfoModalComponent } from '../user-info-modal/user-info-modal.component';
import { User } from '../../../shared/entity/user.entity';
import { IDialogContentBuilder } from '../../../shared/components/delete-dialog-component/dialog-content-builder.interface';
import { UserInfoModalContentBuilder } from '../user-info-modal/user-info-modal-content.builder';

@Component({
  selector: 'user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit {
//************************************************************************************************************************************
  @Input()
  private user : User;

  @Input()
  fontSize = '16px';

  @Input()
  color = '#3f51b5';
  
  private userInfoContentDialogBuilder : IDialogContentBuilder; 
//************************************************************************************************************************************
  constructor(public dialog: MatDialog) {
    
  }
//************************************************************************************************************************************
  ngOnInit() {
    this.userInfoContentDialogBuilder = new UserInfoModalContentBuilder(this.user);
  }
//************************************************************************************************************************************
  openUserInfos(event){
    const dialogRef = this.dialog.open(UserInfoModalComponent, {
      width: '95%',
      minWidth : '100px',
      maxWidth : '600px',
      closeOnNavigation: true,
      data: this.userInfoContentDialogBuilder
    });
  }
//************************************************************************************************************************************
  getUserTitle(){
    return this.user != null ? `  ${this.user.name}  ` : ``;
  }
//************************************************************************************************************************************
  getNgStyle() {
    return {
      'font-size' : this.fontSize,
      'color' : this.color
    }
  }
//************************************************************************************************************************************
}
