import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn } from '@angular/forms';
import validator from 'validator';

@Directive({
    selector: '[isLink]',
    providers: [{provide: NG_VALIDATORS, useExisting: LinkValidatorDirective, multi: true}]
  })
  export class LinkValidatorDirective implements Validator {   
    validate(control: AbstractControl): {[key: string]: any} | null {
        return isLinkValidator()(control)
    }
  }


  export function isLinkValidator(): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      let result : boolean = false;
      if(!control.value)
        return null;
      // vaidate data
      
      if(validator.isFQDN(control.value))
        result=true;
      return !result ? {'isLink': {value: "''"+control.value + " '' Is Not a Link"}} : null;
    };
  }