import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn } from '@angular/forms';
import validator from 'validator';

@Directive({
    selector: '[isEmail]',
    providers: [{provide: NG_VALIDATORS, useExisting: EmailValidatorDirective, multi: true}]
  })
  export class EmailValidatorDirective implements Validator {   
    validate(control: AbstractControl): {[key: string]: any} | null {
        return isEmailValidator()(control)
    }
  }

  /**
   * return null if data is valid else return a ValidationErrors object: 
   * {
   *   isLink : { value : 'XXX is not email'}
   * }
   */
  export function isEmailValidator(): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      let result : boolean = false;
      if(!control.value)
        return null;
      // vaidate data
      if(validator.isEmail(control.value))
        result=true;
      return !result ? {'isEmail': {value: "'"+control.value + "' is not email adress"}} : null;
    };
  }