import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[page-title]'
})
export class PageTitleDirective {

  constructor(el: ElementRef) {
    el.nativeElement.style.fontSize = "35px";
    el.nativeElement.style.fontFamily = "'GoogleSans-Bold' !important;";
    el.nativeElement.style.color = "rgba(0,0,0,.87)";
  }

}
