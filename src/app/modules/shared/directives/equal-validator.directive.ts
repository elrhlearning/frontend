import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn } from '@angular/forms';

@Directive({
    selector: '[isEqual]',
    providers: [{provide: NG_VALIDATORS, useExisting: EqualValidatorDirective, multi: true}]
  })
//*****************************************************************************************************   
  export class EqualValidatorDirective implements Validator {   
//*****************************************************************************************************     
    @Input('isEqual')
    isEqual : string;
//***************************************************************************************************** 
    validate(control: AbstractControl): {[key: string]: any} | null {
        return isEqualValidator(this.isEqual)(control)
    }
//*****************************************************************************************************     
  }

  /**
   * method return null if the value is valid otherwise it return ValidatorFn type
   * @param text text to compare with form control
   */
  export function isEqualValidator(text : string): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      let result : boolean = false;
      if(!control.value)
        return null;
      if(control.value === text)
        result=true;
      return !result ? {'isEqual': {value: `${control.value} not equal to ${text}`}} : null;
    };
  }
//*****************************************************************************************************   