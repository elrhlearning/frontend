import { Directive, Input,ElementRef,OnInit } from '@angular/core';

@Directive({
  selector: '[marginTopAndBottom]'
})
export class MarginTopAndBottomDirective implements OnInit {
  el: ElementRef;
  ngOnInit(): void {
    this.el.nativeElement.style.marginTop  = this.marginTopAndBottom;
    this.el.nativeElement.style.marginBottom  = this.marginTopAndBottom;
  }
  @Input()
  marginTopAndBottom :string;

  constructor(el: ElementRef) { 
    this.el = el;
  }

}
