import { User } from '../../shared/entity/user.entity';
import { TestServiceFactory } from '../../../testing/mocks/test-services.factory';
export class MockUserService {
    private users : User[]; 
    getAll() : User[]{
      if(!this.users){
          this.users = [];
          for(let i = 1; i<= 5; i++){
              let user = new User();
              user.id = i;
              user.name = `Name ${i}`;
              user.lastname = `Lastname ${i}`;
              user.email = `name.lastname${i}@gmail.com `;
              user.password = `user${i}`;
              this.users.push(user);
          }
      }  
      return this.users;  
    }

    getRandomUser() : User{
       return this.getAll()[TestServiceFactory.getRandomInt(0,(this.getAll().length - 1))] ;
    }

    getUser(id : number) : User {
       return  this.getAll().find( u => u.id == id);
    }

    getConnectedUser() : User{
        return  this.getAll().find( u => u.id == 1);
    }

    addUser(user : User){
        this.users.push(user);
    }
}