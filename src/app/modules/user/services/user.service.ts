import { environment } from './../../../../environments/environment';
import { User } from './../../shared/entity/user.entity';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private createAccountURL : string = environment.apiURL+'user/';
  private updateAccountURL : string = environment.apiURL+'user/update';
  private changeCredentialURL : string = environment.apiURL+'user/update/credential';
//*****************************************************************************************************  
  constructor(private readonly http: HttpClient) { }
//*****************************************************************************************************  
  public createAccount(user : User) : Observable<number>{  
    return this.http.post<number>(this.getCreateAccountURL(),user);
  }  
//*****************************************************************************************************  
  public updateAccount(user : User) : Observable<boolean>{  
    return this.http.post<boolean>(this.getUpdateAccountURL(),user);
  }  
//***************************************************************************************************** 
  public changeCredential(user : User) : Observable<boolean>{  
    return this.http.post<boolean>(this.getChangeCredentialtURL(),user);
  }  
//*****************************************************************************************************




  getUpdateAccountURL(){
    return this.updateAccountURL;
  }
  getCreateAccountURL(){
    return this.createAccountURL;
  }
  getChangeCredentialtURL(){
    return this.changeCredentialURL;
  }
}
