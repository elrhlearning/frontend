import { User } from './../../../shared/entity/user.entity';
import { Component, OnInit, EventEmitter, Output, Input, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'user-account-form',
  templateUrl: './user-account-form.component.html',
  styleUrls: ['./user-account-form.component.css']
})
export class UserAccountFormComponent implements OnInit {

  @ViewChild('userForm') 
  form : NgForm;

  showName : boolean = true;
  showLastname : boolean = true;
  showEmail : boolean = true;
  showPhoneNumber : boolean = true;
  showPassword : boolean = true;
  showPasswordConfirmation : boolean = true;
  /**
   * User object
   */
  @Input()
  user : User;
  /**
   * loading snippet
   */
  loading = false;
  /**
   * password confirmation
   */
  passwordConfirmation:string;
  /**
   * used to emit user when its ready
   */
  @Output()
  userReady : EventEmitter<User>;
  formTitle : string = 'Create Account';
  btnTitle : string = 'SignUp';
//****************************************************************************************************  
  constructor() {
    this.userReady = new EventEmitter<User>();
  }
//****************************************************************************************************
  ngOnInit() {
    if(!this.user)
      this.user = new User();
  }
//****************************************************************************************************
  onSubmit(){
    this.userReady.emit(this.user);
  }
//****************************************************************************************************  
  resetAttributes() {
    this.showName  = true;
    this.showLastname  = true;
    this.showEmail  = true;
    this.showPhoneNumber  = true;
    this.showPassword  = true;
    this.showPasswordConfirmation = true;
  }
//****************************************************************************************************
  configureForUpdate(){
    this.btnTitle = 'Save';
    this.formTitle = 'Update Account';
    this.showPassword = false;
    this.showPasswordConfirmation = false;
    this.showEmail = false;
  }
//**************************************************************************************************** 
  configureForChangingPassword(){
    this.btnTitle = 'Save';
    this.formTitle = 'Change Login & Password';
    this.showName = false;
    this.showLastname = false;
    this.showPhoneNumber = false;
  }
//****************************************************************************************************  
}
