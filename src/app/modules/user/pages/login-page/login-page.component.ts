import { User } from './../../../shared/entity/user.entity';
import { AuthService } from './../../../../services/auth.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm} from '@angular/forms';
import { Router, ActivatedRoute,} from '@angular/router';
import { WebsocketService } from '../../../../services/websocket.service';
import { MessengerWebsocketService } from '../../../realtime/services/messenger-websocket.service';
import { MatSnackBar } from '@angular/material';
import { AppRequestErrorResponse, HttpErrorHandler } from 'src/app/services/http-errors-handler.service';

@Component({
  selector: 'login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
//****************************************************************************************************
  constructor(private authService : AuthService,  
              private readonly webSocketService : WebsocketService,
              private readonly messengerWebsocket : MessengerWebsocketService,
              private router: Router) 
  { }
  user : User;
  loading = false;

  @ViewChild('loginForm') loginForm : NgForm;
//****************************************************************************************************
  ngOnInit() {
    // if user is already loggedIn, then back to home page
    if(this.authService.isLoggedIn()){
      this.router.navigate(['/']);
    }
    // to do : check token 
    this.user = new User();
  }
//****************************************************************************************************
  onSubmit(){
    this.loading = true;
    this.authService.login(this.user)
    .subscribe(
      (data : boolean) => {
        if(data){
          this.webSocketService.setClient(this.authService.getLoggedUser());
          if(!this.webSocketService.configure()){
            this.webSocketService.connect();
          }
          this.webSocketService.setClient(this.authService.getLoggedUser());
          if(!this.messengerWebsocket.configure()){
            this.messengerWebsocket.connect();
          }
          setTimeout(() => {
            this.router.navigate(['/']);   
          }, 300);
        }
      }
      ,
      (error)=>{
        this.loading = false;
      }
    );
  }
//****************************************************************************************************
  goToCreateAccountPage(){
    this.router.navigate(['/signup']);
  }  
//****************************************************************************************************  
}
