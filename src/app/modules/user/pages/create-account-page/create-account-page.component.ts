import { UserService } from './../../services/user.service';
import { Router } from '@angular/router';
import { User } from './../../../shared/entity/user.entity';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'create-account-page',
  templateUrl: './create-account-page.component.html',
  styleUrls: ['./create-account-page.component.css']
})
export class CreateAccountPageComponent implements OnInit {
//****************************************************************************************************  
  user : User;
  loading = false;
  passwordConfirmation:string;
//****************************************************************************************************  
  constructor(private router: Router,private readonly userService : UserService) {
    this.user = new User();
  }
//****************************************************************************************************
  ngOnInit() {
  }
//****************************************************************************************************
  createAccount(user : User){
    this.userService.createAccount(user)
    .subscribe(
      (data : number) => {
        user.id = data;
        this.router.navigate(['/login']);
      }
    );
  }
//****************************************************************************************************  
}
