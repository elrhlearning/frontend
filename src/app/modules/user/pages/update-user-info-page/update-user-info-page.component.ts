import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { User } from '../../../shared/entity/user.entity';
import { AuthService } from '../../../../services/auth.service';
import { UserAccountFormComponent } from '../../components/user-account-form/user-account-form.component';
import { UserService } from '../../services/user.service';
import { LoadingCommanderService } from '../../../shared/services/loading-commander.service';

@Component({
  selector: 'update-user-info-page',
  templateUrl: './update-user-info-page.component.html',
  styleUrls: ['./update-user-info-page.component.css']
})
export class UpdateUserInfoPageComponent implements OnInit,OnDestroy {
  user : User;

  @ViewChild('userFormComponent')
  userFormComponent : UserAccountFormComponent;

  constructor(private authService : AuthService,private userService : UserService,private loader : LoadingCommanderService) { }

  ngOnInit() {
    // configure form for update
    this.userFormComponent.configureForUpdate();
    // get logged user
    this.user = this.authService.getLoggedUser();//JSON.parse(JSON.stringify(this.authService.getLoggedUser()));
  }

  updateInfos(user : User){
    this.loader.startLoading();
    this.user = JSON.parse(JSON.stringify(user));
    this.user.token = undefined;
    this.user.password = undefined;
    this.userService.updateAccount(this.user)
    .subscribe(
      (data : boolean) => {
        if(data){
          this.authService.updateLoggedUserInfos(this.user);
        }
      },
      (error) => {this.loader.stopLoading();},
      () => {this.loader.stopLoading()}
    )
  }

  ngOnDestroy(): void {
    // reset form
    this.userFormComponent.resetAttributes();
  }
}
