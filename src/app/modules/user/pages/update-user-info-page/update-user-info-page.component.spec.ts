import { async, ComponentFixture, TestBed, fakeAsync,tick } from '@angular/core/testing';
import { UpdateUserInfoPageComponent } from './update-user-info-page.component';
import { UserService } from '../../services/user.service';
import { AuthService } from '../../../../services/auth.service';
import { TestServiceFactory } from '../../../../testing/mocks/test-services.factory';
import { NO_ERRORS_SCHEMA, DebugElement } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { SidnavContentComponent } from '../../../../components/sidnav/sidnav-content.component';
import { DashboardPageComponent } from '../../../../pages/dashboard-page/dashboard-page.component';
import { UserMenuComponent } from '../../../../components/user-menu/user-menu.component';
import { MessengerNotificationComponent } from '../../../../components/messenger-notification/messenger-notification.component';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '../../../shared/shared.module';
import { UserAccountFormComponent } from '../../components/user-account-form/user-account-form.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';

describe('UpdateUserInfoPageComponent', () => {
//*********************************************************************************************************************  
  // components
  let component: UpdateUserInfoPageComponent;
  let fixture: ComponentFixture<UpdateUserInfoPageComponent>;
  // service
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let userService : UserService;
  let authService  : AuthService;
  let userFormComponent : UserAccountFormComponent;
  let formDebugElement : DebugElement;
  let req;
//*********************************************************************************************************************  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        UserAccountFormComponent,
        UpdateUserInfoPageComponent,
        UserMenuComponent, 
        SidnavContentComponent,  
        DashboardPageComponent, 
        MessengerNotificationComponent
       ],
       // module to import
      imports: [ HttpClientTestingModule,SharedModule,RouterTestingModule,BrowserAnimationsModule],
      // Provide the service-under-test
      providers: [ 
        UserService,
        { provide: AuthService, useFactory : () => { return TestServiceFactory.getAuthServiceMock()} }, 
      ],
      schemas: [ NO_ERRORS_SCHEMA ],
    })
    .compileComponents();
    // get data
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    userService = TestBed.get(UserService);
    authService = TestBed.get(AuthService);
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(UpdateUserInfoPageComponent);
    component = fixture.componentInstance;
    expect(component).toBeTruthy();
    userFormComponent = component.userFormComponent;
    expect(userFormComponent).toBeTruthy();
    formDebugElement = fixture.debugElement.query(By.directive(UserAccountFormComponent));
  }));
//*********************************************************************************************************************
  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });
//*********************************************************************************************************************  
  it('should create instances', () => {
      expect(component).toBeTruthy();
      expect(httpTestingController).toBeTruthy();
      expect(userService).toBeTruthy();
      expect(httpClient).toBeTruthy();
      expect(userFormComponent).toBeTruthy();
      expect(formDebugElement).toBeDefined();
  });
//********************************************************************************************************************* 
  it('should update user', fakeAsync(() => {
      // detect changes in form and component  
      fixture.detectChanges();
      tick(2000);
      expect(userFormComponent).toBeDefined();
      const name = userFormComponent.form.controls['name'];
      const lastname = userFormComponent.form.controls['lastname'];
      const phoneNumber = userFormComponent.form.controls['phone_number'];
      const password = userFormComponent.form.controls['password'];
      expect(name).toBeTruthy();
      expect(lastname).toBeDefined();
      expect(phoneNumber).toBeDefined();
      // password,password confirmation and email inputs dont exists
      expect(password).toBeUndefined();
      // set new value
      let nameValue='new name';
      let lastnameValue='new lastname';
      let phoneNumberValue='0123456789';
      name.setValue(nameValue);
      lastname.setValue(lastnameValue);
      phoneNumber.setValue(phoneNumberValue);
      // submit form 
      let btn = formDebugElement.query(By.css('#login-btn')).nativeElement;
      expect(btn).toBeDefined();
      btn.click();
     // check service
      req = httpTestingController.expectOne(userService.getUpdateAccountURL());
      expect(req.request.method).toEqual('POST');
      req.flush(JSON.stringify(true));
      // detect changes and wait
      fixture.detectChanges();
      tick(2000);
      // check authentification user data
      expect(authService.getMinimizedLoggedUser().name).toEqual(nameValue);
      expect(authService.getMinimizedLoggedUser().lastname).toEqual(lastnameValue);
      expect(authService.getMinimizedLoggedUser().phoneNumber).toEqual(phoneNumberValue);
    //});
  })); 
//*********************************************************************************************************************  
});
