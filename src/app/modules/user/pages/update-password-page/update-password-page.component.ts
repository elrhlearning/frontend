import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { User } from '../../../shared/entity/user.entity';
import { UserAccountFormComponent } from '../../components/user-account-form/user-account-form.component';
import { AuthService } from '../../../../services/auth.service';
import { UserService } from '../../services/user.service';
import { LoadingCommanderService } from '../../../shared/services/loading-commander.service';
import { Router } from '@angular/router';

@Component({
  selector: 'udpate-password-page',
  templateUrl: './update-password-page.component.html',
  styleUrls: ['./update-password-page.component.css']
})
export class UpdatePasswordPageComponent implements OnInit,OnDestroy {
  user : User;

  @ViewChild('userFormComponent')
  userFormComponent : UserAccountFormComponent;

  constructor(private authService : AuthService,private userService : UserService,private loader : LoadingCommanderService,private router : Router) { }

  ngOnInit() {
    // configure form for update
    this.userFormComponent.configureForChangingPassword();
    // get logged user
    this.user = this.authService.getMinimizedLoggedUser();
  }

  changeCredential(user : User){
    this.loader.startLoading();
    this.userService.changeCredential(user)
    .subscribe(
      (data : boolean) => {
        if(data && this.authService.logout()){
          this.router.navigate(['login'])
        }
      },
      (error) => {this.loader.stopLoading();},
      () => {this.loader.stopLoading()}
    )
  }

  ngOnDestroy(): void {
    // reset form
    this.userFormComponent.resetAttributes();
  }
}
