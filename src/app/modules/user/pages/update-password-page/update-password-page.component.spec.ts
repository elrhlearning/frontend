import { async, ComponentFixture, TestBed, fakeAsync,tick } from '@angular/core/testing';
import { UserService } from '../../services/user.service';
import { AuthService } from '../../../../services/auth.service';
import { TestServiceFactory } from '../../../../testing/mocks/test-services.factory';
import { NO_ERRORS_SCHEMA, DebugElement } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { SidnavContentComponent } from '../../../../components/sidnav/sidnav-content.component';
import { DashboardPageComponent } from '../../../../pages/dashboard-page/dashboard-page.component';
import { UserMenuComponent } from '../../../../components/user-menu/user-menu.component';
import { MessengerNotificationComponent } from '../../../../components/messenger-notification/messenger-notification.component';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '../../../shared/shared.module';
import { UserAccountFormComponent } from '../../components/user-account-form/user-account-form.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import { UpdatePasswordPageComponent } from './update-password-page.component';
import { Router } from '@angular/router';


describe('UpdatePasswordPageComponent', () => {
//*********************************************************************************************************************  
  // components
  let component: UpdatePasswordPageComponent;
  let fixture: ComponentFixture<UpdatePasswordPageComponent>;
  // service
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let userService : UserService;
  let authService  : AuthService;
  let userFormComponent : UserAccountFormComponent;
  let formDebugElement : DebugElement;
  let req;
  let routerSpy = jasmine.createSpyObj('Router', ['navigate']);
  let router : Router;
//*********************************************************************************************************************  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        UserAccountFormComponent,
        UpdatePasswordPageComponent,
        UserMenuComponent, 
        SidnavContentComponent,  
        DashboardPageComponent, 
        MessengerNotificationComponent
       ],
       // module to import
      imports: [ HttpClientTestingModule,SharedModule,RouterTestingModule,BrowserAnimationsModule],
      // Provide the service-under-test
      providers: [ 
        UserService,
        { provide: Router,      useValue: routerSpy },
        { provide: AuthService, useFactory : () => { return TestServiceFactory.getAuthServiceMock()} }, 
      ],
      schemas: [ NO_ERRORS_SCHEMA ],
    })
    .compileComponents();
    // get data
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    userService = TestBed.get(UserService);
    authService = TestBed.get(AuthService);
    router = TestBed.get(Router);
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(UpdatePasswordPageComponent);
    component = fixture.componentInstance;
    expect(component).toBeTruthy();
    userFormComponent = component.userFormComponent;
    expect(userFormComponent).toBeTruthy();
    formDebugElement = fixture.debugElement.query(By.directive(UserAccountFormComponent));
  }));
//*********************************************************************************************************************
  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });
//*********************************************************************************************************************  
  it('should create instances', () => {
      expect(component).toBeTruthy();
      expect(httpTestingController).toBeTruthy();
      expect(userService).toBeTruthy();
      expect(httpClient).toBeTruthy();
      expect(userFormComponent).toBeTruthy();
      expect(formDebugElement).toBeDefined();
  });
//********************************************************************************************************************* 
  it('should update credential', fakeAsync(() => {
      // detect changes in form and component  
      fixture.detectChanges();
      tick(1000);
      expect(userFormComponent).toBeDefined();
      const name = userFormComponent.form.controls['name'];
      const password = userFormComponent.form.controls['password'];
      const email = userFormComponent.form.controls['login'];
      expect(name).toBeUndefined();
      expect(email).toBeDefined();
      expect(password).toBeDefined();
      // set new value
      let passwordValue='06414681';
      let emailValue='test.1@gmail.com';
      password.setValue(passwordValue);
      email.setValue(emailValue);
      // now password confirmation input will appear
      fixture.detectChanges();
      tick(1000);
      const passwordConfirmation = userFormComponent.form.controls['password_confirmation'];
      expect(passwordConfirmation).toBeDefined();
      passwordConfirmation.setValue(passwordValue);
      // submit form 
      let btn = formDebugElement.query(By.css('#login-btn')).nativeElement;
      expect(btn).toBeDefined();
      btn.click();
      // check service
      req = httpTestingController.expectOne(userService.getChangeCredentialtURL());
      expect(req.request.method).toEqual('POST');
      req.flush(JSON.stringify(true));
      // detect changes and wait
      fixture.detectChanges();
      tick(1000);
      const spy = router.navigate as jasmine.Spy;
      expect(spy).toHaveBeenCalledWith(['login']);
      // check authentification user data
      expect(authService.getLoggedUser()).toBeUndefined();
  })); 
//*********************************************************************************************************************  
});
