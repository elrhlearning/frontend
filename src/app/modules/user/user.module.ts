import { UserAccountFormComponent } from './components/user-account-form/user-account-form.component';
import { SharedModule } from './../shared/shared.module';
import { CreateAccountPageComponent } from './pages/create-account-page/create-account-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { NgModule } from '@angular/core';
import { UserRoutingModule } from './user-routing.module';
import { UpdateUserInfoPageComponent } from './pages/update-user-info-page/update-user-info-page.component';
import { UpdatePasswordPageComponent } from './pages/update-password-page/update-password-page.component';

@NgModule({
  declarations: [LoginPageComponent,CreateAccountPageComponent,UserAccountFormComponent,UpdateUserInfoPageComponent, UpdatePasswordPageComponent],
  imports: [
    UserRoutingModule,
    SharedModule,
  ],
  exports : [
    LoginPageComponent,CreateAccountPageComponent,UserAccountFormComponent,
  ]
})
export class UserModule { }
