import { LoginPageComponent } from './pages/login-page/login-page.component';
import { CreateAccountPageComponent } from './pages/create-account-page/create-account-page.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UpdateUserInfoPageComponent } from './pages/update-user-info-page/update-user-info-page.component';
import { DashboardPageComponent } from '../../pages/dashboard-page/dashboard-page.component';
import { AuthGuard } from '../../guards/auth.guard';
import { UpdatePasswordPageComponent } from './pages/update-password-page/update-password-page.component';

const routes: Routes = [
  {
    path : 'login',
    component : LoginPageComponent
  },
  {
    path : 'signup',
    component : CreateAccountPageComponent
  },
  {
    path : 'user',
    component : DashboardPageComponent,
    canActivate: [AuthGuard],
    children : [
      {
        path : 'edit', 
        component : UpdateUserInfoPageComponent,
      },
      {
        path : 'credential', 
        component : UpdatePasswordPageComponent,
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
