import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardPageComponent } from './pages/dashboard-page/dashboard-page.component';
import { LoginPageComponent } from './modules/user/pages/login-page/login-page.component';
import { CreateAccountPageComponent } from './modules/user/pages/create-account-page/create-account-page.component';
import { reviewModuleRoutes } from './modules/reviews/reviews-routing.module';
import { realtimeModuleRoutes } from './modules/realtime/realtime-routing.module';
import { ReviewEntryComponent } from './modules/reviews/review-entry.component';
import { AuthGuard } from './guards/auth.guard';
import { RealtimeEntryComponent } from './modules/realtime/realtime-entry.component';


const routes: Routes = [
  {
    path : '',
    component : DashboardPageComponent,
    canActivate: [AuthGuard],
    children : [
      {
        path : '',
        redirectTo : 'review',
        pathMatch : 'full'
      },
      {
        path : 'review',
        component : ReviewEntryComponent,
        children : [
          ... reviewModuleRoutes
        ]
      },
      {
        path : 'messenger',
        component : RealtimeEntryComponent,
        children : [
          ... realtimeModuleRoutes
        ]
      }
    ]
  },
  {
    path : 'login',
    component : LoginPageComponent
  },
  {
    path : 'signup',
    component : CreateAccountPageComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{
    paramsInheritanceStrategy: 'always',
    onSameUrlNavigation: 'reload'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
