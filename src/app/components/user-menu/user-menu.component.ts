import { Router } from '@angular/router';
import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { WebsocketService } from '../../services/websocket.service';

@Component({
  selector: 'user-menu-component',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.css']
})
export class UserMenuComponent implements OnInit {

  constructor(private authService : AuthService,private router : Router,private readonly webSocketService:WebsocketService) { }

  ngOnInit() {
  }

  logout(){
    if(this.authService.logout()){
      this.webSocketService.disconnect();
      this.router.navigate(['/login']);
    }
  }

  editInfos(){
    this.router.navigate(['/user/edit']);
  }

  changeCredential(){
    this.router.navigate(['/user/credential']);
  }
}
