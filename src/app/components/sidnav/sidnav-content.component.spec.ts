import { async, ComponentFixture, TestBed  } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA, Input, HostListener, Directive, DebugElement } from '@angular/core';
import { SidnavContentComponent } from '../sidnav/sidnav-content.component';
import { RouterTestingModule } from '@angular/router/testing';
import { By, BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

/**
 * we create this directive to replace the default one 
 */
@Directive({
  selector: '[routerLink]'
})
export class RouterLinkDirectiveStub {
  @Input('routerLink') linkParams: any;
  navigatedTo: any = null;

  @HostListener('click')
  onClick() {
    this.navigatedTo = this.linkParams;
  }
}


describe('MessengerNotificationComponent', () => {
  let component: SidnavContentComponent;
  let fixture: ComponentFixture<SidnavContentComponent>;
  let routerLinks: RouterLinkDirectiveStub[];
  let linkDes: DebugElement[];
//********************************************************************************************
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations : [SidnavContentComponent, RouterLinkDirectiveStub],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents().then(() => {
      fixture = TestBed.createComponent(SidnavContentComponent);
      component    = fixture.componentInstance;
    });
  }));
//********************************************************************************************
  beforeEach(() => {
    fixture.detectChanges(); // trigger initial data binding

    // find DebugElements with an attached RouterLinkStubDirective
    linkDes = fixture.debugElement
      .queryAll(By.directive(RouterLinkDirectiveStub));

    // get attached link directive instances
    // using each DebugElement's injector
    routerLinks = linkDes.map(de => de.injector.get(RouterLinkDirectiveStub));
  });
//********************************************************************************************
  it('can instantiate the component', () => {
    expect(component).not.toBeNull();
  });
//********************************************************************************************
  it('can get RouterLinks from template', () => {
    expect(linkDes.length).toBe(2, 'should have 2 routerLinks directive');
    expect(routerLinks.length).toBe(2, 'should have 2 routerLinks');
    expect(routerLinks[0].linkParams).toBe('/review');
    expect(routerLinks[1].linkParams).toBe('/messenger');
  });
//********************************************************************************************
  it('can click Heroes link in template', () => {
    const heroesLinkDe = linkDes[1];   // DebugElement
    const heroesLink = routerLinks[1]; // directive
    expect(heroesLink.navigatedTo).toBeNull('should not have navigated yet');
    heroesLinkDe.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(heroesLink.navigatedTo).toBe('/messenger');
  });
//********************************************************************************************
});

