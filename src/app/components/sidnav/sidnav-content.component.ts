import { AuthService } from './../../services/auth.service';
import { Component,ViewChild,HostListener } from '@angular/core';
import { MatSidenav } from '@angular/material';

@Component({
  selector: 'sidnav-content-component',
  templateUrl: './sidnav-content.component.html',
  styleUrls: ['./sidnav-content.component.css']
})
export class SidnavContentComponent  {
  items : Object;
  constructor(){
    this.items = [
      {name : 'All reviews',path : '/review',requirAuth : true},
      {name : 'Conversations',path : '/messenger',requirAuth : true},
    ];
  }
}
