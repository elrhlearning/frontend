import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessengerNotificationComponent } from './messenger-notification.component';
import { UserMenuComponent } from '../user-menu/user-menu.component';
import { SidnavContentComponent } from '../sidnav/sidnav-content.component';
import { DashboardPageComponent } from '../../pages/dashboard-page/dashboard-page.component';
import { SharedModule } from '../../modules/shared/shared.module';
import { RealtimeModule } from '../../modules/realtime/realtime.module';
import { RouterTestingModule } from '@angular/router/testing';
import { MessengerService } from '../../modules/realtime/services/messenger.service';
import { TestServiceFactory } from '../../testing/mocks/test-services.factory';

describe('MessengerNotificationComponent', () => {
//********************************************************************************************  
  let component: MessengerNotificationComponent;
  let fixture: ComponentFixture<MessengerNotificationComponent>;
//********************************************************************************************
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations : [UserMenuComponent, SidnavContentComponent,  DashboardPageComponent, MessengerNotificationComponent],
      imports: [ SharedModule,RealtimeModule,RouterTestingModule],
      providers: [
        { provide: MessengerService, useFactory : () => {return TestServiceFactory.getMessengerServiceMock();} }
      ]
    })
    .compileComponents();
  }));
//********************************************************************************************
  beforeEach(() => {
    fixture = TestBed.createComponent(MessengerNotificationComponent);
    component = fixture.componentInstance;
  });
//********************************************************************************************
  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(component.getColor()).toEqual('accent');
    component.messengerService.nbNewMessages = 0;
    expect(component.getColor()).toEqual('primary');
    expect(component.getNbNewMessages()).toEqual(0);
  });
//********************************************************************************************  
});
