import { Component, OnInit } from '@angular/core';
import { MessengerService } from '../../modules/realtime/services/messenger.service';
import { Router } from '@angular/router';

@Component({
  selector: 'messenger-notif-component',
  templateUrl: './messenger-notification.component.html',
  styleUrls: ['./messenger-notification.component.css']
})
export class MessengerNotificationComponent implements OnInit {

  constructor(public readonly messengerService : MessengerService,private readonly router : Router) {

  }

  ngOnInit() {
  }



  getNbNewMessages() : number {
    return this.messengerService.nbNewMessages;
  }

  getColor() : string {
    if(this.getNbNewMessages() > 0)
      return 'accent';
    return 'primary';
  }

  goToMessenger(){
    this.router.navigate(['messenger']);
  }
}
