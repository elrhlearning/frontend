import { AuthService } from './../services/auth.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot,CanActivate, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import { WebsocketService } from '../services/websocket.service';
import { MessengerWebsocketService } from '../modules/realtime/services/messenger-websocket.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private authService : AuthService,
              private readonly webSocketService : WebsocketService,
              private readonly messengerWebsocketService : MessengerWebsocketService,
              private router: Router){

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    if(this.authService.isLoggedIn()){
      this.webSocketService.setClient(this.authService.getLoggedUser());
      if(!this.webSocketService.configure()){
        this.webSocketService.connect();
      }
      this.messengerWebsocketService.setClient(this.authService.getLoggedUser());
      if(!this.messengerWebsocketService.configure()){
        this.messengerWebsocketService.connect();
      }
      return true;
    }      
    this.router.navigate(['/login']);
  }

}
