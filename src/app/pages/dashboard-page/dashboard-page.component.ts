import { Component,ViewChild,OnInit} from '@angular/core';
import { MatSidenav } from '@angular/material';
import {BreakpointObserver,Breakpoints,BreakpointState} from '@angular/cdk/layout';

@Component({
  selector: 'dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.css']
})
export class DashboardPageComponent implements OnInit{
  title = 'Planet Reviews';
  sidNavType : string = "side";
  sidenaveState : boolean = false;
  @ViewChild('sidenav') sidenav: MatSidenav;
  public innerWidth: any;

  constructor(public breakpointObserver: BreakpointObserver){
  }

  ngOnInit(): void {  
     // when screen is small
     this.breakpointObserver
      .observe([Breakpoints.XSmall,Breakpoints.Small])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          this.sidenaveState = false;
          this.sidNavType="over";
        }
      });
      // when screen is large
      this.breakpointObserver
      .observe([Breakpoints.Medium,Breakpoints.XLarge,Breakpoints.Web])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          this.sidenaveState = true;
          this.sidNavType="side";
        }
      });
    
  }
}
